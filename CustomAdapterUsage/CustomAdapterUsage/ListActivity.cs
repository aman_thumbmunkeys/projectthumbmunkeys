﻿using System.Collections.Generic;
using System.Xml.Schema;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace CustomAdapterUsage
{
    [Activity(Label = "ListActivity")]
    public class ListActivity : Activity
    {
        public string[] Items = { "Aman", "Deepak", "Chirag", "Sid", "Sidjr", "Harsh", "Roshan", "Pranoy", "Tanuj" };
        public List<Person> list => GetPersons();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EmployeeList);
            ListView listView;
            listView = FindViewById<ListView>(Resource.Id.List);
            listView.Adapter = new ScreenAdapter(this, list);
            listView.ItemClick += OnListItemClick;
        }

        public void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var itemValue = list[e.Position];
            Intent intent = new Intent(this, typeof(SecondActivity));
            intent.PutExtra("name", itemValue.Name);
            intent.PutExtra("surname", itemValue.Surname);
            StartActivity(intent);
        }

        private List<Person> GetPersons()
        {
            var listPersons = new List<Person>();
            for (int i = 0; i < 100; i++)
            {
                listPersons.Add(new Person("Aman" + i, "Sharma" + i));
            }

            return listPersons;
        }
    }
}