﻿using Android.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

namespace CustomAdapterUsage
{
    public class ScreenAdapter : BaseAdapter<Person>
    {
        private List<Person> _items;
        Activity context;
        public ScreenAdapter(Activity context, List<Person> list) : base()
        {
            this.context = context;
            _items = list;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder;
            var item = _items[position];
            var view = convertView;

            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.CustomView, null);
                holder = new ViewHolder
                {
                    TvName = (TextView)view.FindViewById(Resource.Id.Text1),
                    TvSurname = (TextView)view.FindViewById(Resource.Id.Text2)
                };
                view.Tag = holder;
            }
            else
            {
                holder = view.Tag as ViewHolder;
            }

            if (holder != null)
            {
                holder.TvName.Text = item.Name;
                holder.TvSurname.Text = item.Surname;
            }

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override Person this[int position]
        {
            get { return _items[position]; }
        }
    }
}