﻿using Android.App;
using Android.OS;
using Android.Widget;

namespace CustomAdapterUsage
{
    [Activity(Label = "SecondActivity")]
    public class SecondActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HelloEmployee);
            var text1 = Intent.GetStringExtra("name") ?? "Data not available";
            var text2 = Intent.GetStringExtra("surname") ?? "Data not available";

            var showText = FindViewById<TextView>(Resource.Id.textView1);
            showText.Text = showText.Text+" " + text1 + text2;
        }
    }
}