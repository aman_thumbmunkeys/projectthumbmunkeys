﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Content;

namespace CustomAdapterUsage
{
    [Activity(Label = "LogIn Details:", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            Button submitButton = FindViewById<Button>(Resource.Id.Button1);
            submitButton.Click += (s,e) =>
            {
                StartActivity(typeof(ListActivity));
            };
        }
    }
}

