﻿namespace CustomAdapterUsage
{
    public class Person
    {
        public string Name;
        public string Surname;

        public Person(string name,string surname)
        {
            Name = name;
            Surname = surname;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetSurname()
        {
            return Surname;
        }

        public void SetName(string name)
        {
            Name = name;
        }

        public void SetSurname(string surname)
        {
            Surname = surname;
        }
    }
}