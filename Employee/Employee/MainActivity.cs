﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace Employee
{
    [Activity(Label = "Employee", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ListActivity
    {
        public string[] Items;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            //SetContentView (Resource.Layout.Main);
           // var listView = FindViewById<ListView>(Resource.);
            Items=new string[]{"Aman","Chirag", "Deepak","Roshan","SidJr","Harsh","Sid","Pranoy","Tanuj" };
           ListAdapter=new ArrayAdapter<string>(this,Resource.Layout.Main,Items);
        }
    }
}

