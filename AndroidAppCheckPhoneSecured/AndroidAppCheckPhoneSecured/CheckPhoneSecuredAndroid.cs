﻿using Android.App;
using Android.Content;

namespace AndroidAppCheckPhoneSecured
{
    public class DeviceCheck
    {
        public bool CheckDeviceSecured(MainActivity activity)
        {
            return ((KeyguardManager)activity.GetSystemService(Context.KeyguardService)).IsKeyguardSecure;
        }
    }
}
