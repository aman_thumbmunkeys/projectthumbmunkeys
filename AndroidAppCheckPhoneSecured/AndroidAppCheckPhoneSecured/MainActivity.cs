﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace AndroidAppCheckPhoneSecured
{
    [Activity(Label = "CheckPhoneSecuredAndroidApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            if (new DeviceCheck().CheckDeviceSecured(this))
            {
                Toast.MakeText(this, "Secured With password/Pin/Pattern..", ToastLength.Long).Show();
            }
            else
            {
                Toast.MakeText(this, "Not secured ...", ToastLength.Long).Show();
            }
        }
    }
}

