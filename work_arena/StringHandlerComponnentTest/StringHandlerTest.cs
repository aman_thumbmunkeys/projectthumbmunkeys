﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace StringHandlerComponnentTest
{
    [TestClass]
    public class StringHandlerTest
    {
        [TestMethod]
        public void InsertSpacesValidOrNot()
        {
            var player = new StringHandler();
            var input = "AmanSharma";
            var expected = "Aman Sharma";

          
            var actual = InputSpaces(input);
            Assert.AreEqual(expected, actual);
        }

        public void InsertSpacesWithExistingSpace()
        {
            var input = "Aman SharmaAman";
            var expected = "Aman Sharma Aman";

            var handler = new StringHandler();
            var actual = handler.InputSpaces(input);
            Assert.AreEqual(expected, actual);
        }
    }
}
