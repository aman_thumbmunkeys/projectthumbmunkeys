﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkArena.PluralSightCourses.LinqCourse;


namespace LinqQueryTests
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        [TestMethod]
        public void FindingExistingCustomerCheck()
        {
            var customerRepository=new CustomerRepository();
            var customerList = customerRepository.RetrieveFromCustomer();
            var result = customerRepository.FindCustomer(customerList, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(2,result.CustomerId);
            Assert.AreEqual("AAAA",result.FirstName);

        }

        [TestMethod]
        public void FindingExistingCustomerUsingLinqQuerySyntaxCheck()
        {
            var customerRepository = new CustomerRepository();
            var customerList = customerRepository.RetrieveFromCustomer();
            var result = customerRepository.FindCustomer(customerList, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.CustomerId);
            Assert.AreEqual("AAAA", result.FirstName);

        }

        [TestMethod]
        public void FindingExistingCustomerUsingLinqMethodSyntaxCheck()
        {
            var customerRepository = new CustomerRepository();
            var customerList = customerRepository.RetrieveFromCustomer();
            var result = customerRepository.FindCustomer(customerList, 2);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.CustomerId);
            Assert.AreEqual("AAAA", result.FirstName);

        }

    }
}
