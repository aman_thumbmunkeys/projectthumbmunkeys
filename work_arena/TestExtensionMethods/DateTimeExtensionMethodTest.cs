﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkArena.PluralSightCourses.ExtensionMethodCourse;

namespace TestExtensionMethods
{
    [TestClass]
    public class DateTimeExtensionMethodTest
    {
        [TestMethod]
        public void ToLegacyFormat1()
        {
            var dateTime=new DateTime(1925,12,31);
            Assert.AreEqual("025-12-31", dateTime.ToLegacyFormat());
        }
        
        [TestMethod]
        public void ToLegacyFormat2()
        {
            var dateTime=new DateTime(2017,12,31);
            Assert.AreEqual("117-12-31",dateTime.ToLegacyFormat());
        }

        [TestMethod]
        public void ToLegacyFormatString()
        {
            var name = "Aman Sharma";
            Assert.AreEqual("AMAN,SHARMA",name.ToLegacyFormatString());
        }
    }
}
