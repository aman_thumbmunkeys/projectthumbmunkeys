﻿using System;

namespace WorkArena.Overloading
{
    class MethodOverloadingByArguments
    {
        public void Area(int pi, int r)
        {
            Console.WriteLine("Area Circle is: {0}", pi * r * r);
        }
        public void Area(int length, int breadth, int height)
        {
            Console.WriteLine("Area cuboid is : {0}", length * breadth * height);
        }

        public void Area(int side)
        {
            Console.WriteLine("Area Square is : {0}", side * side);
        }
    }
}
