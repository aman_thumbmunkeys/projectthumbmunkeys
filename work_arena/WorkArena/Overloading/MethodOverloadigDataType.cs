﻿using System;

namespace WorkArena.Overloading
{
    class MethodOverloadigDataType
    {
        public void Area(float pi, int r)
        {
            Console.WriteLine("Area Circle is: {0}", pi * r * r);
        }
        public void Area(int length, int breadth)
        {
            Console.WriteLine("Area rectangle is : {0}", length * breadth);
        }

        public void Area(double side,int height)
        {
            Console.WriteLine("Area trapezium is : {0}", 0.5 * side * height);
        }
    }
}
