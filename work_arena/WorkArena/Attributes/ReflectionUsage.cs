﻿using System;
using System.Reflection;
namespace WorkArena.Attributes
{
    class ReflectionUsage
    {
        public void UsingReflection()
        {
            Type t = typeof(DebugInfo);
            Console.WriteLine(t.FullName);
            Console.WriteLine(t.Name);
            Console.WriteLine(t.Namespace);

            Console.WriteLine("To print properties details : ");
            PropertyInfo[] properties = t.GetProperties();
            foreach (var property in properties)
            {
                Console.WriteLine(property.PropertyType.Name + " " + property.Name);
            }
            Console.WriteLine();

            Console.WriteLine("To print method related details : ");
            MethodInfo[] methodInfos = t.GetMethods();
            foreach (var method in methodInfos)
            {
                Console.WriteLine("Method name is: {0} ", method.Name);
                Console.WriteLine("method return type is : {0} ", method.ReturnType.Name);
                Console.WriteLine(method.GetParameters());
            }
            Console.WriteLine();

            Console.WriteLine("To print Constructor details : ");
            ConstructorInfo[] constructorInfos = t.GetConstructors();
            foreach (var constructor in constructorInfos)
            {
                Console.WriteLine(constructor.Name);
                Console.WriteLine(constructor.ToString());
            }
        }
    }
}
