﻿using System;
using System.CodeDom;
using System.Reflection;
namespace WorkArena.Attributes
{
    [DebugInfo(45, "Aman", "25 june", Message = "Code running good")]
    class UsingDebugInfo
    {
        [DebugInfo(90, "chirag", "27 june", Message = "Code not running good")]
        public void  UsingDebugInfoMethod()
        {
            Console.WriteLine("we used attribute ... ");
        }
    }
}
