﻿using System;
using System.Collections.Generic;

namespace WorkArena.Attributes
{
    class AttributesUsage
    {
        [Obsolete("use Add(List<int> Numbers)Method", true)]
        public int Add(int x, int y)
        {
            return x + y;
        }
        public int Add(List<int> Numbers)
        {
            int sum = 0;
            foreach (var number in Numbers)
            {
                sum = sum + number;
            }
            return sum;
        }
    }
}
