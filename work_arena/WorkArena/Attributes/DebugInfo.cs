﻿using System;
namespace WorkArena.Attributes
{
    [AttributeUsage(AttributeTargets.Class |
                    AttributeTargets.Constructor |
                    AttributeTargets.Field |
                    AttributeTargets.Method |
                    AttributeTargets.Property,
        AllowMultiple = true)]
    class DebugInfo : Attribute
    {
        public int BugNo { get; }

        public string Developer { get; }

        public string LastReview { get; }

        public string Message { get; set; }

        public DebugInfo(int bugNo, string developer, string lastReview)
        {
            BugNo = bugNo;
            Developer = developer;
            LastReview = lastReview;
        }
    }
}
