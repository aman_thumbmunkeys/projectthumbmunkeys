﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WorkArena.Attributes
{
    class TestAttribute
    {
        public void AccessAttribute()
        {
            Type t = typeof(UsingDebugInfo);
            //MethodInfo[] methodInfo = t.GetMethods();
            foreach (Object attributes in t.GetCustomAttributes(false))
            {
                var dbi = (DebugInfo)attributes;
                if (null != dbi)
                {
                    Console.WriteLine("Bug no: {0}", dbi.BugNo);
                    Console.WriteLine("Developer: {0}", dbi.Developer);
                    Console.WriteLine("Last Reviewed: {0}", dbi.LastReview);
                    Console.WriteLine("Remarks: {0}", dbi.Message);
                }
            }
            foreach (MethodInfo method in t.GetMethods())
            {
                foreach (Attribute methodAttributes in method.GetCustomAttributes(false))
                {
                    DebugInfo dbi = methodAttributes as DebugInfo;
                    if (null != dbi)
                    {
                        Console.WriteLine($"bug no is {dbi.BugNo}");
                        Console.WriteLine("Developer: {0}", dbi.Developer);
                        Console.WriteLine("Last Reviewed: {0}", dbi.LastReview);
                        Console.WriteLine("Remarks: {0}", dbi.Message);
                    }

                }
            }
        }
    }

}
