﻿using System;

namespace WorkArena.Abstraction
{
    class Teacher : School
    {
        int[] arr = { 78, 90, 87, 56, 89 };
        public override void Marks()
        {
            Console.WriteLine("Marks in Maths,English,Chemistry,Physics and Hindi are:");
            foreach (var marks in arr)
            {
               Console.Write("{0} ",marks); 
            }
        }
        public override void Grades()
        {
            Console.WriteLine("Grades of child are:  ");
            foreach (var marks in arr)
            {
                if (marks >= 90)
                {
                    Console.WriteLine("Grade is A");
                }
                else if (marks <= 89 && marks >= 80 )
                {
                    Console.WriteLine("Grade is AB");
                }
                else if(  marks<=79 && marks>=70)
                {
                    Console.WriteLine("Grade is B");
                }
                else if (marks <= 69 && marks >= 60)
                {
                    Console.WriteLine("Grade is BC");
                }
                else if (marks <= 59 && marks >= 50)
                {
                    Console.WriteLine("Grade is C");
                }
                else
                {
                    Console.WriteLine("Garde is D");
                }
            }
            Console.ReadLine();
        }
    }
}
