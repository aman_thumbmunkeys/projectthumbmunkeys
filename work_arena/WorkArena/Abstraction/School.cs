﻿namespace WorkArena.Abstraction
{
    public abstract class School
    {
        public abstract void Marks();
        public abstract void Grades();
    }
}
