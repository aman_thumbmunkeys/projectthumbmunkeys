﻿using System;

namespace WorkArena.DateAndTime
{
    class DateAndTimesProgram
    {
        public void ShowDateAndTime()
        {
            DateTime myValue = DateTime.Now;
            Console.WriteLine(myValue.ToString());
            Console.WriteLine(myValue.ToShortDateString());
            Console.WriteLine(myValue.ToShortTimeString());
            Console.WriteLine(myValue.ToLongDateString());
            Console.WriteLine(myValue.ToLongTimeString());
            Console.WriteLine(myValue.AddDays(3).ToLongDateString());
            Console.WriteLine(myValue.AddHours(2).ToLongTimeString());
            Console.WriteLine(myValue.AddDays(-3).ToLongDateString());
            Console.WriteLine(myValue.AddHours(-2).ToLongTimeString());
            Console.WriteLine(myValue.Month);

            DateTime myJoining=new DateTime(2017,6,7);
            Console.WriteLine(myJoining.ToShortDateString());

            DateTime myBirthday = DateTime.Parse("1994/12/31");
            TimeSpan myAge = DateTime.Now.Subtract(myBirthday);
            Console.WriteLine(myAge.Days);
            Console.ReadLine();
        }
    }
}
