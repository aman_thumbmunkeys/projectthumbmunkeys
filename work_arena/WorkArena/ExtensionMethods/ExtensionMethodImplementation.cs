﻿namespace WorkArena.ExtensionMethods
{
    public static class ExtensionMethodImplementation
    {
        public static string ChangeFirstLetterCase(this string name)
        {
            if (name.Length > 0)
            {
                char[] charArray = name.ToCharArray();
                charArray[0] = char.IsUpper(charArray[0]) ? char.ToLower(charArray[0]) : char.ToUpper(charArray[0]);
                return new string(charArray);
            }
            return name;
        }
    }
}
