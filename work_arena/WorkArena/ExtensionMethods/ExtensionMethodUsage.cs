﻿using System;

namespace WorkArena.ExtensionMethods
{
    class ExtensionMethodUsage
    {
        public ExtensionMethodUsage()
        {
            Console.WriteLine("Enter String to change first case: ");
            string strName=Console.ReadLine();
            string result = strName.ChangeFirstLetterCase();
            Console.WriteLine(result);
        }
    }
}
