﻿using System;

namespace WorkArena.CSharpPrograms
{
    class ArmstrongNumber
    {
        public void ArmstrongNumberCheck(int number)
        {
            int num = number;
            int mod, sum = 0;
            while (num > 0)
            {
                mod = num % 10;
                sum = sum + mod * mod * mod;
                num = num / 10;
            }
            if (sum == number)
            {
                Console.WriteLine("Armstrong number !! ");
            }
            else
            {
                Console.WriteLine("Not  Armstrong !! ");
            }
        }
    }
}
