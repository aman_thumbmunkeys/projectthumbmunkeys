﻿using System;

namespace WorkArena.CSharpPrograms
{
    class PrimeNumber
    {
        public void PrimeNumberCheck(int number)
        {
            int flag = 0;
            for (int i = 2; i < Math.Sqrt(number); i++)
            {
                if (number % i == 0)
                {
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                Console.WriteLine("Prime Number !! ");
            }
            else
            {
                Console.WriteLine("Not Prime Number !!");
            }
        }
    }
}
