﻿using System;

namespace WorkArena.CSharpPrograms
{
    class FactorialOfNumber
    {
        public void FindFactorial(int number)
        {
            int fact = 1;
            for (int i = 1; i <= number; i++)
            {
                fact = fact * i;
            }
            Console.WriteLine($"Factorial is : {fact}");
        }
    }
}
