﻿using System;

namespace WorkArena.CSharpPrograms
{
    class DecimalToBinary
    {
        public void ConvertDecimalToBinary()
        {
            int number = int.Parse(Console.ReadLine());
            int[] binary = new int[10];
            int num = number;
            int k = 0;
            while (num > 0)
            {
                binary[k++] = num % 2;
                num = num / 2;
            }

            for (int i = k; i >=0; i--)
            {
                Console.Write($"{binary[i]} ");
            }
            Console.WriteLine();
        }
    }
}
