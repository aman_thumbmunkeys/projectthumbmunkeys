﻿using System;

namespace WorkArena.CSharpPrograms
{
    class FibonacciSeries
    {
        public void FibonacciPrint(int limit)
        {
            int n1=0, n2=1, n3;
            Console.Write($"{n1} {n2} ");
            for (int i = 2; i < limit; i++)
            {
                n3 = n2 + n1;
                Console.Write($"{n3} ");
                n1 = n2;
                n2 = n3;
            }
            Console.WriteLine();
        }
    }
}
