﻿using System;

namespace WorkArena.CSharpPrograms
{
    class ReverseNumber
    {
        public void Reverse(int number)
        {
            int n = number;
            int k, rev = 0,sum=0;
            while (n > 0)
            {
                k = n % 10;
                sum = sum + k;
                rev = rev * 10 + k;
                n = n / 10;
            }
            Console.WriteLine($"Reversed number is : {rev}");
            Console.WriteLine($"Sum of Digits is : {sum}");
        }
    }
}
