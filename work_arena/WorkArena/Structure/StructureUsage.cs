﻿using System;

namespace WorkArena.Structure
{
    struct StructureUsage
    {
        public string Name;
        public string CollegeName;
        public int RollNumber;
        public void GetDetails(string name, string collegeName, int rollNumber)
        {
            Name = name;
            CollegeName = collegeName;
            RollNumber = rollNumber;
        }

        public void Display()
        {
            Console.WriteLine(Name);
            Console.WriteLine(CollegeName);
            Console.WriteLine(RollNumber);
        }
    }
}
