﻿using System;
using System.CodeDom;

namespace WorkArena.DynamicPolymorphism
{
    class RectangleD : ShapeD
    {
        public RectangleD(int a = 0, int b = 0) : base(a, b)
        {
        }

        public override void Area()
        {
            Console.WriteLine("Area is: {0}", Width * Height);
        }
    }
}
