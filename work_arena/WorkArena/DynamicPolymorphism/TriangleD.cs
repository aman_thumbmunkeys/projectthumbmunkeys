﻿using System;

namespace WorkArena.DynamicPolymorphism
{
    class TriangleD : ShapeD
    {
        public TriangleD(int a = 0, int b = 0) : base(a, b)
        {
        }

        public override void Area()
        {
            Console.WriteLine("Area of triangle is: {0}", Width * Height / 2);
        }
    }
}
