﻿using System;

namespace WorkArena.DynamicPolymorphism
{
    class ShapeD
    {
        protected int Width, Height;
        public ShapeD(int a=0, int b=0)
        {
            Width = a;
            Height = b;
        }

        public virtual void Area()
        {
            Console.WriteLine("Area is..");
        }
    }
}
