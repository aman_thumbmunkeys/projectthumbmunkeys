﻿namespace WorkArena.DelegatesAndEvents
{
    class ResultEvent
    {
        public event CalulatorDelegate CalculatorEvent;

        public void NotifyUser(int x,int y)
        {
            CalculatorEvent?.Invoke(x,y);
        }
    }
}
