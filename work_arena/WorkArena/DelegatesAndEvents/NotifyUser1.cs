﻿using System;

namespace WorkArena.DelegatesAndEvents
{
    class NotifyUser1
    {
        public static void NotifyHandler(int x, int y)
        {
            Console.WriteLine("User 1 is Notified !! ...\n");
        }
    }
}
