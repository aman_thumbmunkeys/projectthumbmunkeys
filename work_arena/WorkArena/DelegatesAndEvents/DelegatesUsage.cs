﻿using System;
public delegate void CalulatorDelegate(int num1,int num2);
namespace WorkArena.Delegates
{
    class DelegatesUsage
    {
        public int Value;
        public void Add(int num1, int num2)
        {

            Value = num1 + num2;
            Console.WriteLine("Sum is {0}\n", Value);
        }

        public void Multiply(int num1, int num2)
        {
            Value = num1 * num2;
            Console.WriteLine("Multiplication is : {0}\n", Value);
        }

        public void Subtract(int num1, int num2)
        {
            Value = Math.Abs(num1 - num2);
            Console.WriteLine("Subtraction is : {0}\n",Value);
        }
    }
}
