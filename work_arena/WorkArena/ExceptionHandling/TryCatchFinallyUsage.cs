﻿using System;

namespace WorkArena.ExceptionHandling
{
    class TryCatchFinallyUsage
    {
        int[] arr = { 12, 23, 445, 67, 8, 9, 0, 7, 6 };
        public int GetNumber()
        {
            Console.WriteLine("Enter index of array to find element: ");
            int index = int.Parse(Console.ReadLine());
            if (index < 0 || index >= arr.Length)
            {
                throw new IndexOutOfRangeException();
            }
            return arr[index];
        }

        public void CatchException()
        {
            TryCatchFinallyUsage throwUsage = new TryCatchFinallyUsage();
            try
            {
                int value = throwUsage.GetNumber();
                Console.WriteLine($"Reterived value is {value}");
                Console.WriteLine();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine($"Exception found is : {e.GetType().Name} and index was out of bounds!!");
                Console.WriteLine();
            }
            finally
            {
                Console.WriteLine("Executed successfully !!");
            }
        }
    }
}
