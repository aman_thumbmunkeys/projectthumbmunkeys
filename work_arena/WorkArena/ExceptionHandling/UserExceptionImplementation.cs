﻿using System;

namespace WorkArena.ExceptionHandling
{
    class UserExceptionImplementation
    {
        public int CabNumber;
        public void GetCab()
        {
            Console.WriteLine("Enter the number of cab you have booked from uber: ");
            CabNumber = int.Parse(Console.ReadLine());
            try
            {
                if (CabNumber < 10 && CabNumber >= 1)
                {
                    Console.WriteLine("You have booked from uber \n Cab will reach in 5 minutes !!");
                    Console.WriteLine();
                }
                else
                {
                    throw new UserDefinedExceptionUsage("Exception not booked from UBER !!");
                }
            }

            catch (UserDefinedExceptionUsage usde)
            {
                Console.WriteLine(usde.Message);
                Console.ReadLine();
            }
        }
    }
}
