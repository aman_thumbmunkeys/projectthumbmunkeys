﻿using System;

namespace WorkArena.ExceptionHandling
{
    class UserDefinedExceptionUsage : Exception
    {
        public UserDefinedExceptionUsage(string message) : base(message)
        {
            Console.WriteLine("Exception ...............\n");
        }
    }
}
