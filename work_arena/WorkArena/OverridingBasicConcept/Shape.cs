﻿using System;

namespace WorkArena.OverridingBasicConcept
{
    class Shape
    {
        public virtual void Draw()
        {
            Console.WriteLine("Draw some shape !!");
        }
    }
}
