﻿using System;

namespace WorkArena.OverridingBasicConcept
{
    class Rectangle:Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Rectangle is drawn !!");
        }
            
    }
}
