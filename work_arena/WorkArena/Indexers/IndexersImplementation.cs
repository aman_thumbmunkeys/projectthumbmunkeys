﻿using System;
using System.CodeDom.Compiler;

namespace WorkArena.Indexers
{
    class IndexersImplementation
    {
        public static int Size = 10;
        private string[] _nameList = new string[Size];
        public IndexersImplementation()
        {
            for (int i = 0; i < Size; i++)
            {
                _nameList[i] = "N.A";
            }
        }

        public string this[int index]
        {
            get
            {
                string temp;
                if (index >= 0 && index <= Size - 1)
                {
                    temp = _nameList[index];
                }
                else
                {
                    temp = "";
                }
                return temp;
            }
            set
            {
                if (index >= 0 && index <= Size - 1)
                {
                    _nameList[index] = value;
                }
            }
        }
    }
}
