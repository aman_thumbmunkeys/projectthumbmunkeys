﻿using System;
using System.Text.RegularExpressions;

namespace WorkArena.RegularExpressions
{
    class RegularExpressionUsage
    {
        public RegularExpressionUsage()
        {
            Console.WriteLine("Enter text to match : ");
            string text =Console.ReadLine();
            ShowMatch(text,@"^[a-zA-Z]{3}[0-9]{8}");
        }
        public void ShowMatch(string text, string expr)
        {
            Console.WriteLine("Matched exp are :");
            MatchCollection matchCollection = Regex.Matches(text, expr);
            foreach (var matchText in matchCollection)
            {
                Console.WriteLine(matchText);
            }
            Console.WriteLine();
        }
    }
}
