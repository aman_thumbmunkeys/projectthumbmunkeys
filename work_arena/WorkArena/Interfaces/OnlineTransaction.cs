﻿using System;
namespace WorkArena.Interfaces
{
    class OnlineTransaction : ITransaction
    {
        public string BankName;
        public double AmountTransfer;
        public string TransactionCode;
        public OnlineTransaction()
        {
            TransactionCode = " ";
            AmountTransfer = 0.0;
            BankName = " ";
        }
        public OnlineTransaction(string transactionCode, string bankName, int amountTransfer)
        {
            TransactionCode = transactionCode;
            AmountTransfer = amountTransfer;
            BankName = bankName;
        }
        public double GetAmount()
        {
            return AmountTransfer;
        }
        public void ShowTransaction()
        {
            Console.WriteLine("Bank name where transfer is done:{0}",BankName);
            Console.WriteLine("Amount transfered is : {0}",GetAmount());
            Console.WriteLine("TRANSACTION CODE IS : {0}",TransactionCode);
            Console.ReadLine();
        }
    }
}
