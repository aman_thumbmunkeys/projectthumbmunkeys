﻿namespace WorkArena.Interfaces
{
    interface ITransaction
    {
        void ShowTransaction();
        double GetAmount();
    }
}
