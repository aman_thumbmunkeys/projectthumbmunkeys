﻿using System;
namespace WorkArena.OperatorOverloading
{
    class CuboidVolume
    {
        private int _length;
        private int _breadth;
        private int _height;
        public void GetVolume()
        {
            Console.WriteLine("Volume is: {0}", _length * _breadth * _height);
        }
        public void SetLength(int length)
        {
            _length = length;
        }
        public void SetBreadth(int breadth)
        {
            _breadth = breadth;
        }
        public void SetHeight(int height)
        {
            _height = height;
        }
        public static CuboidVolume operator +(CuboidVolume obj1, CuboidVolume obj2)
        {
            CuboidVolume cuboidVolume = new CuboidVolume();
            cuboidVolume._length = obj1._length + obj2._length;
            cuboidVolume._breadth = obj1._breadth + obj1._breadth;
            cuboidVolume._height = obj1._height + obj2._height;
            return cuboidVolume;
        }
    }
}
