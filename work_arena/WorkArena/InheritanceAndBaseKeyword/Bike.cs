﻿using System;

namespace WorkArena.Inheritance
{
    class Bike
    {
        public int speed;
        public int Time=2;
        public int Distance = 90;
        public void CalculateSpeed()
        {
            
            if(Time!=0)
            speed= Distance / Time;
            if(speed !=0)
            Console.WriteLine("Speed of bike is : {0}km/hr",speed);
        }

        public void BikeType()
        {
            Console.WriteLine("New model is honda !!");
        }
    }
}
