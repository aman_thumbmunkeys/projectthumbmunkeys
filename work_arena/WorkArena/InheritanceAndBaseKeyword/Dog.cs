﻿using System;

namespace WorkArena.Inheritance
{
    class Dog : Animal
    {
        string Color = "white";
        public void Bark()
        {
            Console.WriteLine("Dog barks..");
            //Console.WriteLine(base.Color);
            Console.WriteLine(Color);
        }
        public void Run()
        {
            Console.WriteLine("Dog runs fastly!!");
        }
    }
}
