﻿using System.Collections.Generic;
using System.Data.Odbc;
using StringHandlerComponent;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    public class Customer
    {
        public Customer() : this(0)
        {

        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CustomerId { get; private set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public List<Address> AddressList { get; set; }

        public Customer(int customerId)
        {
            CustomerId = customerId;
            var AddressList = new List<Address>();
        }

        public bool Validate()
        {
            bool isValid = true;
            if (FirstName != null && LastName == null && Email != null)
            {
                isValid = !string.IsNullOrWhiteSpace(FirstName);
                isValid = !string.IsNullOrWhiteSpace(Email);
                isValid = !string.IsNullOrWhiteSpace(LastName);
            }
            if (FirstName == null && LastName != null && Email != null)
            {
                isValid = !string.IsNullOrWhiteSpace(LastName);
                isValid = !string.IsNullOrWhiteSpace(Email);
                isValid = !string.IsNullOrWhiteSpace(FirstName);
            }
            if (Email == null && FirstName != null && LastName != null)
            {
                isValid = !string.IsNullOrWhiteSpace(LastName);
                isValid = !string.IsNullOrWhiteSpace(FirstName);
                isValid = !string.IsNullOrWhiteSpace(Email);
            }
            if (FirstName == null && LastName == null && Email == null)
            {
                isValid = !string.IsNullOrWhiteSpace(LastName);
                isValid = !string.IsNullOrWhiteSpace(FirstName);
                isValid = !string.IsNullOrWhiteSpace(Email);
            }
            return isValid;
        }

        public override string ToString()
        {
            return FirstName;
        }
    }
}
