﻿using System;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    class BankingTransactions:BankingApplication
    {
        public override void MiniStatement()
        {
            Console.WriteLine("Mini statement is : {0}",LastTransactionAmount);
        }

        public override void BalanceAfterWithdrawl()
        {
            int withdraw = BalanceAvailable-LastTransactionAmount;
            Console.WriteLine("Balance is : {0}",withdraw);
        }

        public BankingTransactions(int balanceAvailable, int lastAmount) : base(balanceAvailable, lastAmount)
        {
        }
    }
}
