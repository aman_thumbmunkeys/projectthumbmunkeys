﻿namespace WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface
{
    public class ProductForInterface :ILoggable
    {
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public int CurrentPrice { get; set; }

        public string Log()
        {
            var logString = ProductName + " " + ProductDescription + " " + CurrentPrice;
            return logString;
        }
    }
}
