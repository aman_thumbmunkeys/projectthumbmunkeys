﻿using System;
using System.Collections.Generic;

namespace WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface
{
    public class LoggingService 
    {
        public static void WriteToFile(List<ILoggable> changedItems)
        {
            foreach (var item in changedItems)
            {
                Console.WriteLine(item.Log());
            }
        }
    }
}
