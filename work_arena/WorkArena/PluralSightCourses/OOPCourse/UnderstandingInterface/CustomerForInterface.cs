﻿namespace WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface
{
    public class CustomerForInterface : ILoggable
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Log()
        {
            var logString = Email + " " + FirstName + " " + LastName;
            return logString;
        }
    }
}
