﻿namespace WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface
{
    public interface ILoggable
    {
        string Log();
    }
}
