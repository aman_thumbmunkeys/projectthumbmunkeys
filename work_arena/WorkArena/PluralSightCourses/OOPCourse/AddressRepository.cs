﻿using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    public class AddressRepository
    {
        public Address Retrieve(int addressId)
        {
            Address address = new Address(addressId);
            if (addressId == 1)
            {
                address.State = "Haryana";
                address.StreetLine1 = "Jmd megapolis";
                address.City = "Gurgaon";
            }
            return address;
        }
        public IEnumerable<Address> RetrieveByCustomerId(int customerId)
        {
            var addressList = new List<Address>();
            Address address = new Address(1)
            {
                StreetLine1 = "Jmd megapolis",
                City = "Gurgaon",
                State = "Haryana"
            };
            Address address2 = new Address(2)
            {
                StreetLine1 = "jmd megapolis",
                City = "Gurgaon",
                State = "Haryana"
            };
            return addressList;
        }
}
}
