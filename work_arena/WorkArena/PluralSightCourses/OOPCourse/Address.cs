﻿using System.Collections;
using System.Collections.Generic;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    public class Address
    {
        public int AddressId { get; private set; }
        public string StreetLine1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public Address(int addressId)
        {
            AddressId = addressId;
        }
        
    }
}
