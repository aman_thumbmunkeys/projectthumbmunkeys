﻿using System.Linq;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    public class CustomerRepositoryUsage
    {
        private static AddressRepository addressRepository { get; set; }

        public CustomerRepositoryUsage()
        {
            var addressRepository=new AddressRepository();
        }
        public  Customer Retrievedata(int customerId)
        {
            Customer customer = new Customer(customerId)
            {
                AddressList = addressRepository.RetrieveByCustomerId(customerId).ToList()
            };
            if (customerId == 1)
            {
                customer.Email = "aman@thumbmunkeys.com";
                customer.FirstName = "Aman";
                customer.LastName = "Sharma";
            }
            return customer;
        }
    }
}
