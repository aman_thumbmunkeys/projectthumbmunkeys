﻿using System;
using System.Threading;

namespace WorkArena.PluralSightCourses.OOPCourse
{
    class BankingApplication
    {
        public int BalanceAvailable { get; set; }
        public int LastTransactionAmount { get; set; }
        public BankingApplication(int balanceAvailable, int lastAmount)
        {
            BalanceAvailable = balanceAvailable;
            LastTransactionAmount = lastAmount;
        }
        public virtual void BalanceAfterWithdrawl()
        {
            Console.WriteLine("Amout withdraw...");
        }

        public virtual void MiniStatement()
        {
            Console.WriteLine("Mini statement produced ...");
        }
    }
}
