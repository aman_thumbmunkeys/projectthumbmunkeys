﻿using System;

namespace WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse
{
    public delegate int DelegateProcess(int x, int y);
    class ProcessDataUsingCustomDelegate
    {
        public void DataProcess(int x, int y, DelegateProcess delegateProcess)
        {
            var result = delegateProcess(x, y);
            Console.WriteLine("result is :{0}" ,result);
        }

        public void ActionProcess(int x, int y, Action<int, int> action)
        {
            action(x, y);
            Console.WriteLine("Action delegate is invoked .... ");
        }

        public void FuncProcess(int x, int y, Func<int, int, int> func)
        {
            var result = func(x, y);
            Console.WriteLine("Result of func is : {0}",result);
        }

        public void StringFunc(string x, string y, Func<string, string, string> str)
        {
            var result = str(x, y);
            Console.WriteLine($"resulted string is ; {result}");
        }
    }
}
