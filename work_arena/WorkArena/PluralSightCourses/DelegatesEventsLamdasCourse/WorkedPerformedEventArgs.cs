﻿namespace WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse
{
    public class WorkedPerformedEventArgs:System.EventArgs
    {
        public int Hours { get; set; }
        public WorkType WorkType { get; set; }

        public WorkedPerformedEventArgs(int hours,WorkType workType)
        {
            Hours = hours;
            WorkType = workType;
        }
    }
}
