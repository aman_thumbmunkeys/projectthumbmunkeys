﻿using System;

namespace WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse
{
    //public delegate void WorkPerformedHandler1(int hours, WorkType work);
    class Worker
    {
        public event EventHandler<WorkedPerformedEventArgs> WorkEvent ;
        public event EventHandler WorkCompletedEvent;

        public void DoWork(int hours, WorkType workType)
        {
            for (int i = 0; i < hours; i++)
            {
                System.Threading.Thread.Sleep(1000);
                OnWorkPerformed(i + 1, workType);
            }
            OnWorkCompleted();
        }

        protected virtual void OnWorkPerformed(int hours, WorkType workType)
        {
            var del = WorkEvent as EventHandler<WorkedPerformedEventArgs>;
            if (del != null)
            {
                del(this, new WorkedPerformedEventArgs(hours,workType));
            }
        }

        protected virtual void OnWorkCompleted()
        {
            (WorkCompletedEvent as EventHandler)?.Invoke(this, EventArgs.Empty);
        }

        public static void WorkShow(object sender, WorkedPerformedEventArgs e)
        {
            Console.WriteLine($"{e.Hours},{e.WorkType}");
        }

        public static void WorkCompleted(object sender,EventArgs e)
        {
            Console.WriteLine("Worker is done !! ....");
        }
    }
}
