﻿using System;

namespace WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse
{
    public delegate void WorkPerformedHandler(int hours, WorkType work);
    class CreatingDelegates
    {
        public void Work1(int hours, WorkType work)
        {
            Console.WriteLine($"Work done is in hours: {hours},{work}");
        }

        public void Work2(int hours, WorkType work)
        {
            Console.WriteLine($"work done is in hours: {hours},{work}");

        }

        public void Work3(int hours, WorkType work)
        {
            Console.WriteLine($"Work done is in hours {hours},{work}");
        }
    }
    public enum WorkType
    {
        GoTomeetings,
        GolfPlayed,
        CricketPlayed
    };
}
