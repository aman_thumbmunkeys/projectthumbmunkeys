﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace WorkArena.PluralSightCourses.LinqCourse
{
    public class CustomerRepository
    {
        public Customer FindCustomer(List<Customer> customerList, int customerId)
        {
            Customer foundCustomer = null;
            foreach (var customer in customerList)
            {
                if (customer.CustomerId == customerId)
                {
                    foundCustomer = customer;
                    break;
                }
            }
            return foundCustomer;
        }

        public Customer FindCustomerUsingLinqQuerySyntax(List<Customer> customerList, int customerId)
        {
            Customer foundCustomer = null;
            var query = from c in customerList
                        where c.CustomerId == customerId
                        select c;
            foundCustomer = query.FirstOrDefault();
            return foundCustomer;
        }

        public Customer FindCustomerUsingLinqMethodSyntax(List<Customer> customerList, int customerId)
        {
            Customer foundCustomer = null;
            foundCustomer = customerList.Where(c =>
            c.CustomerId == customerId)
            .Skip(1)
            .FirstOrDefault();

            return foundCustomer;
        }

        public void FindCustomerDetails(List<Customer> customers, int customerId)
        {
            Customer foundCustomer = customers.FirstOrDefault(c =>
            {
                Console.WriteLine(c.FirstName);
                return c.CustomerId == customerId;
            });
        }

        public IEnumerable<Customer> SortByName(List<Customer> customerList)
        {
            return customerList.OrderBy(c => c.LastName).ThenBy(c => c.FirstName);
        }
        public List<Customer> RetrieveFromCustomer()
        {
            var customerList = new List<Customer>
            {
                new Customer()
                {
                    CustomerId = 1,
                    FirstName = "Aman",
                    LastName = "Sharma",
                    Email = "aman@thumbmunkeys.com"

                },
                new Customer()
                {
                    CustomerId = 2,
                    FirstName = "AAAA",
                    LastName = "SSSS",
                    Email = "ASD@thumbmunkeys.com"
                }
            };
            return customerList;
        }
    }
}
