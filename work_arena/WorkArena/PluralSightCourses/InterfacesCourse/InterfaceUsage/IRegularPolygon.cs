﻿namespace WorkArena.PluralSightCourses.InterfacesCourse.InterfaceUsage
{
    interface IRegularPolygon
    {
        int NumberOfSides { get; set; }
        int SideLength { get; set; }

        void GetPerimeter();
        void GetArea();
    }
}
