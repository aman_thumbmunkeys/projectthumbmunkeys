﻿using System;

namespace WorkArena.PluralSightCourses.InterfacesCourse.InterfaceUsage
{
    class OctagonToImplementPolygon : IRegularPolygon
    {
        public int NumberOfSides { get; set; }
        public int SideLength { get; set; }

        public OctagonToImplementPolygon(int length)
        {
            NumberOfSides = 8;
            SideLength = length;
        }

        public void GetPerimeter()
        {
            Console.WriteLine($"perimeter is : {NumberOfSides * SideLength}");
        }

        public void GetArea()
        {
            Console.WriteLine($"Area is : {SideLength * SideLength}");
        }

    }
}
