﻿using System;

namespace WorkArena.PluralSightCourses.InterfacesCourse.ConcreteClassUsage
{
    class RegularPolygon
    {
        public int NumberOfSides { get; set; }
        public int SideLength { get; set; }

        public RegularPolygon(int numberOfSides, int sideLength)
        {
            NumberOfSides = numberOfSides;
            SideLength = sideLength;
        }

        public void GetPerimeter()
        {
            Console.WriteLine($"perimeter is : {NumberOfSides* SideLength}");
        }

        public virtual void GetArea()
        {
            throw new NotImplementedException();
        }
    }
}
