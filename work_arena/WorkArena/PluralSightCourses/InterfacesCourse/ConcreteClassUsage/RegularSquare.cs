﻿using System;
namespace WorkArena.PluralSightCourses.InterfacesCourse.ConcreteClassUsage
{
    class RegularSquare : RegularPolygon
    {
        public RegularSquare(int length) : base(4, length) { }

        public override void GetArea()
        {
            Console.WriteLine($"Area of square is : {SideLength * SideLength}");
        }
    }
}

