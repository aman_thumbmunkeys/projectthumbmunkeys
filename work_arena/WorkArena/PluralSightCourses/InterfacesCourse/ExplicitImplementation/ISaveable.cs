﻿namespace WorkArena.PluralSightCourses.InterfacesCourse.ExplicitImplementation
{
    interface ISaveable
    {
        void Save();
    }
}
