﻿namespace WorkArena.PluralSightCourses.InterfacesCourse.ExplicitImplementation
{
    interface IPrintable
    {
        void Save();
    }
}
