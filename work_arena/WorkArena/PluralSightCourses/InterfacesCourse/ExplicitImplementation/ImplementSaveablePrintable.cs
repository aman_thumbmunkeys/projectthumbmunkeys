﻿using System;

namespace WorkArena.PluralSightCourses.InterfacesCourse.ExplicitImplementation
{
    class ImplementSaveablePrintable:ISaveable,IPrintable
    {
        public void Save()
        {
            string name = "ThumbmunkeysLtd";
            char[] nameChar = name.ToCharArray();
            string nameString = null;
            char[] names=new char[10];
            int k = 0;
            foreach (var letter in nameChar)
            {
                if (char.IsUpper(letter))
                {
                    nameString += char.ToLower(letter);
                    
                }
                else
                nameString += letter;
            }
            Console.WriteLine(nameString);
        }

        public void Load()
        {
            Console.WriteLine("Something is loaded..  ");
        }
    }
}
