﻿using System;

namespace WorkArena.PluralSightCourses.InterfacesCourse.AbstractClassUsage
{
    public class RegularTriangle : AbstractPolygonUsage
    {
        public RegularTriangle(int length) : base(3, length) { }

        public override void GetArea()
        {
            Console.WriteLine($"Area of triangle: {(Math.Sqrt(3) / 4) * SideLength * SideLength}");
        }
    }
}
