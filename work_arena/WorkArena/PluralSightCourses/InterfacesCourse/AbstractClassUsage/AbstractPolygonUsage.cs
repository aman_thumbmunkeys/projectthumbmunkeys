﻿using System;

namespace WorkArena.PluralSightCourses.InterfacesCourse.AbstractClassUsage
{
    public abstract class AbstractPolygonUsage
    {
        public int NumberOfSides { get; set; }
        public int SideLength { get; set; }

        protected AbstractPolygonUsage(int numberOfSides, int sideLength)
        {
            NumberOfSides = numberOfSides;
            SideLength = sideLength;
        }

        public void GetPerimeter()
        {
            Console.WriteLine($"perimeter is : {NumberOfSides * SideLength}");
        }

        public abstract void GetArea();

    }
}
