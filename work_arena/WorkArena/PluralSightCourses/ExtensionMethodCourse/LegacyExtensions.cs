﻿using System;

namespace WorkArena.PluralSightCourses.ExtensionMethodCourse
{
    public static class LegacyExtensions
    {
        public static string ToLegacyFormat(this DateTime dateTime)
        {
            return dateTime.Year > 1930 ? dateTime.ToString("1yy-MM-dd") : dateTime.ToString("0yy-MM-dd");
        }

        public static string ToLegacyFormatString(this string name)
        {
            var parts = name.ToUpper().Split(' ');
            return parts[0] + "," + parts[1];
        }
    }
}
