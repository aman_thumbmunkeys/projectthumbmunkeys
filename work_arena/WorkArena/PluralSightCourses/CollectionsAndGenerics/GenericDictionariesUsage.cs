﻿using System;
using System.Collections.Generic;

namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class GenericDictionariesUsage
    {
        public GenericDictionariesUsage()
        {
            var cityDictionary = new Dictionary<string, string>
            {
                {"SPN","Shahjahanpur"},
                {"BE","Bareilly"},
                {"JP","jaipur"}
            };
            var companyDetails = new Dictionary<int, CustomerDetails>
            {
                {1, new CustomerDetails()
                {
                    Id = 4,Name = "Aman",CompanyDetails = "Thumbmunkeys"
                }},
                {2, new CustomerDetails()
                {
                   Id = 3,Name = "Abc",CompanyDetails = "SSSS"
                }}
            };
            Console.WriteLine("Keys in Dictionary are :");
            foreach (var key in companyDetails.Keys)
            {
                Console.Write($"{key} ");
            }
            Console.WriteLine();
            foreach (var values in cityDictionary.Values)
            {
                Console.Write($"{values} ");
            }
            foreach (var elements in cityDictionary)
            {
                var companyValues = elements.Value;
                var companyKeys = elements.Key;
                Console.WriteLine($"key : {companyKeys} Value: {companyValues}");
            }
            foreach (var details in companyDetails)
            {
                Console.WriteLine(details.Value.Name);
                Console.WriteLine(details.Value.CompanyDetails);
                Console.WriteLine(details.Value.Id);

            }
        }
    }
}
