﻿using System;
using System.Collections.Generic;

namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class GenericListUsage
    {
        public GenericListUsage()
        {
            var vendors = new List<Vendors>();
            var vendor = new Vendors()
            {
                VendorId = 1,
                CompanyName = "ThimbMynkesys",
                EmployeeName = "dasda"
            };
            vendors.Add(vendor);
            vendor= new Vendors()
            {
                VendorId = 2,
                CompanyName = "Thiesys",
                EmployeeName = "asa"
            };
            vendors.Add(vendor);
            foreach (var variable in vendors)
            {
                Console.WriteLine(variable.VendorId);
                Console.WriteLine(variable.CompanyName);
                Console.WriteLine(variable.EmployeeName);
            }
        }
        public void ListImplementation()
        {
            List<string> names = new List<string> { "A1", "A2", "A3" };
            names.Add("c3");
            names.Add("C5");
            names.Remove("C5");
            foreach (var name in names)
            {
                Console.Write(name + " ");
            }
            Console.WriteLine();
        }
    }
}
