﻿using System;

namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class ArrayCollectionUsage
    {
        public ArrayCollectionUsage()
        {
            string[] subjects = {"Maths", "English", "Science", "Hindi"};
            foreach (var elementOfSubject in subjects)
            {
                Console.WriteLine(elementOfSubject);
            }

            for (int i = 0; i < subjects.Length; i++)
            {
                Console.WriteLine(subjects[i].ToLower());
            }
            Console.WriteLine($"Index of maths elements is : {Array.IndexOf(subjects,"Maths")}");
            subjects.SetValue("Conputer Science",3);
            Console.WriteLine($"Value set at index 3 is : {subjects[3]}");
        }
    }
}
