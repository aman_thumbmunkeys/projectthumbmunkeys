﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class GenericMethodUsage
    {
        public void Swap<TSwap>(TSwap a, TSwap b)
        {
            TSwap temp = a;
            a = b;
            b = temp;
            Console.WriteLine($"a is : {a} and b is {b}");
        }

        public TReturn Addition<TReturn>(TReturn a, TReturn b,Func<TReturn,TReturn,TReturn> add)
        {
            var result = add(a, b);
            return result;
        }
    }
}
