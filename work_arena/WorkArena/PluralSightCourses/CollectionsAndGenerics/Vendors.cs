﻿namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class Vendors
    {
        public int VendorId { get; set; }
        public string CompanyName { get; set; }
        public string EmployeeName { get; set; }
    }
}
