﻿using System;
public delegate void NumberChanger<T>(T number1, T number2);
namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class GenericDelegateUsage
    {
        public void Addition(int n1, int n2)
        {
            int result = n1 + n2;
            Console.WriteLine($"adiition is : {result}");
        }

        public void Multiplication(int n1, int n2)
        {
            int result = n1 * n2;
            Console.WriteLine($"Multiplication  is : {result}");

        }

        public void Swap(int number1, int number2)
        {
            int temp = number1;
            number1 = number2;
            number2 = temp;
            Console.WriteLine($"Two numbers after swap are : {number1},{number2}");
        }
    }
}
