﻿namespace WorkArena.PluralSightCourses.CollectionsAndGenerics
{
    class CustomGenericArray<T>
    {
        public T[] Array;

        public CustomGenericArray(int size)
        {
            Array = new T[size];
        }
        public void SetItem(int index, T value)
        {
            Array[index] = value;
        }

        public T GetItem(int index)
        {
            return Array[index];
        }
    }
}
