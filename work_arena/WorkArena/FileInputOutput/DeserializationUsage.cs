﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WorkArena.FileInputOutput
{
    class DeserializationUsage
    {
        public DeserializationUsage()
        {
            FileStream fileStream = new FileStream(@"C: \Users\Aman Sharma\Documents\File.txt", FileMode.OpenOrCreate);
            var binaryFormatter = new BinaryFormatter();
            Student student = (Student) binaryFormatter.Deserialize(fileStream);
            Console.WriteLine("Roll no is ; {0}",student.Rollno);
            Console.WriteLine("name is : {0}",student.Name);
            fileStream.Close();
        }
    }
}
