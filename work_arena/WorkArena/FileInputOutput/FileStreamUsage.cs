﻿using System;
using System.IO;

namespace WorkArena.FileInputOutput
{
    class FileStreamUsage
    {
        public FileStreamUsage()
        {
            string path = @"C:\Users\Aman Sharma\Documents\f.txt";
            if (File.Exists(path))
            {
                Console.WriteLine("Path Exist..........");
                File.Delete(path);
            }
            FileStream fw = new FileStream(@"C: \Users\Aman Sharma\Documents\aman.txt", FileMode.Create);
            StreamWriter sw = new StreamWriter(fw);
            sw.WriteLine("File input output !! ");
            sw.Close();
            fw.Close();
        }
        public void ReadFile()
        {
            FileStream fr = new FileStream(@"C: \Users\Aman Sharma\Documents\aman.txt", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fr);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            fr.Close();
            sr.Close();
        }
    }
}
