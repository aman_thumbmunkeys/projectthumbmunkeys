﻿using System;

namespace WorkArena.FileInputOutput
{
    [Serializable]
    public class Student
    {
        public string Name;
        public int Rollno;
        public Student(int rollno,string name)
        {
            Rollno = rollno;
            Name = name;
        }
    }
}
