﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace WorkArena.FileInputOutput
{
    public class SerializationUsage
    {
        public SerializationUsage()
        {
            try
            {
                var student = new Student(21, "Aman");
                var fileStream = new FileStream(@"C: \Users\Aman Sharma\Documents\File.txt",FileMode.OpenOrCreate);
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, student);
                fileStream.Close();
            }
            catch (Exception e)
            {
                
            }
        }
    }
}
