﻿using System;

namespace WorkArena.WhileForUse
{
    class WhileForUsage
    {
        public void Display()
        {
            while (MainMenu())
            {
            }
        }

        public bool MainMenu()
        {
            Console.WriteLine("Choose an option 1 for guess game \n or 2  exit: ");
            string result = Console.ReadLine();
            if (result == "1")
            {
                GuessGame();
                return true;
            }
            return false;
        }

        public void GuessGame()
        {
            Console.WriteLine("guessing game");

            Random myRandom = new Random();
            int randomNumber = myRandom.Next(1, 11);
            int guess = 0;
            bool incorrect = true;

            do
            {
                Console.WriteLine("Guess a number between 1 and 10  : ");
                string result = Console.ReadLine();
                guess++;
                if (result == randomNumber.ToString())
                {
                    incorrect = false;
                }
                else
                {
                    Console.WriteLine("Incorrect");
                }
            } while (incorrect);

            Console.WriteLine("Correct and guesses are {0}:", guess);
            Console.ReadLine();
        }
    }
}
