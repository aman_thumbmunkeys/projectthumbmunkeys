﻿using System;
using System.Data.Common;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace WorkArena.LinqQuery
{
    class LinkUsage
    {
        public void CheckVowel()
        {
            var sample = "I enjoy Eating Food";
            var result = from character in sample
                         select character;
            foreach (var item in result)
            {
                Console.Write("{0} ", item);
            }
            var result1 = from character in sample.ToLower()
                          where character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u'
                          orderby character
                          select character;
            Console.WriteLine();
            foreach (var vowel in result1)
            {
                Console.Write("{0} ", vowel);
            }
            Console.WriteLine();
            var result2 = from character in sample.ToLower()
                          where character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u'
                          orderby character
                          group character by character;
            foreach (var groupVowel in result2)
            {
                Console.WriteLine("{0} - {1} ", groupVowel.Key, groupVowel.Count());
            }
        }
    }
}
