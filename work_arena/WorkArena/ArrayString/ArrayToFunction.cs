﻿
using System;

namespace WorkArena.ArrayString
{
    class ArrayToFunction
    {
        public void ArrayMaxElement(int[] arr)
        {
            int max = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            Console.WriteLine("maximum number is {0}:", max);
            Console.ReadLine();
        }
    }
}
