﻿using System;

namespace WorkArena.ArrayString
{
    public class ExampleArray
    {
        public void PrintArray()
        {
            int i;
            int[] numbers = new int[6];
            Console.WriteLine("Enter elements of array: ");
            for (i = 0; i < 6; i++)
            {
                numbers[i] = Convert.ToInt32(Console.ReadLine());
            }

            i = 0;
            while (i < 6)
            {
                Console.WriteLine(numbers[i]);
                i++;
            }
            Console.ReadLine();
        }

        public void PrintString()
        {
            string[] names = { "Aman", "Chirag", "sss" };
            int temp = -10;
            string city = "INDIA";

            foreach (string name in names)
            {
                Console.WriteLine(name);
            }
            string output = String.Format("In \'{0}\' the temperature is {1}", city, temp);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public void ReverseString()
        {
            string sentence = "you are aman sharma";
            char[] charArray = sentence.ToCharArray();
            Array.Reverse(charArray);

            foreach (char name in charArray)
            {
                Console.Write(name);
            }
            Console.ReadLine();
        }

        public void StringReverse()
        {
            char[] firstNameArray = { };
            char[] lastNameArray = { };
            char[] cityNameArray = { };

            Console.WriteLine("Enter your first name: ");
            string firstName = Console.ReadLine();

            Console.WriteLine("Enter your last name: ");
            string lastName = Console.ReadLine();

            Console.WriteLine("Enter your city name: ");
            string cityName = Console.ReadLine();

            if (!string.IsNullOrEmpty(firstName))
            {
                firstNameArray = firstName.ToCharArray();
                Array.Reverse(firstNameArray);
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                lastNameArray = lastName.ToCharArray();
                Array.Reverse(lastNameArray);
            }

            if (!string.IsNullOrEmpty(cityName))
            {
                cityNameArray = cityName.ToCharArray();
                Array.Reverse(cityNameArray);
            }

            foreach (char namePrint in firstNameArray)
            {
                Console.Write(namePrint);
            }
            Console.Write(" ");

            foreach (char namePrint in lastNameArray)
            {
                Console.Write(namePrint);
            }
            Console.Write(" ");

            foreach (char namePrint in cityNameArray)
            {
                Console.Write(namePrint);
            }

            Console.ReadLine();
        }
    }
}
