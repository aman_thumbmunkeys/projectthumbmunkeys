﻿using System.Collections.Generic;

namespace WorkArena.YieldKeyword
{
    class YieldUsage
    {
        public IEnumerable<int> Fibonacci(int n)
        {
            int n1 = 0, n2 = 1,sum=0;
            for (int i = 0; i < n; i++)
            {
                sum = n1 + n2;
                n1 = n2;
                n2 = sum;
                yield return sum;
            }
        }
    }
}
