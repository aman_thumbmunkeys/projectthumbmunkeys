﻿using System;
using System.Collections.Generic;
namespace WorkArena.Collections
{
    class LinkedListUsage
    {
        public void LinkedListFunctions()
        {
            int[] numbers = {1, 2, 3, 4, 5, 6, 7,6,90};
            LinkedList<int> linkedList=new LinkedList<int>(numbers);
            linkedList.AddFirst(23);
            Console.WriteLine("Move the first node to last ....");
            LinkedListNode<int> store = linkedList.First; 
            linkedList.RemoveFirst();
            linkedList.AddLast(store);
            LinkedListNode<int> find = linkedList.FindLast(6);
            linkedList.AddAfter(store,56);
            linkedList.AddBefore(store, 89);
            foreach (var num in linkedList)
            {
                Console.Write("{0} ", num);
            }
        }
    }
}
