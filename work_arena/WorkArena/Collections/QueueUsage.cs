﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WorkArena.Collections
{
    class QueueUsage
    {
        public void QueueImplementation()
        {
            Queue queue = new Queue();
            queue.Enqueue('a');
            queue.Enqueue('b');
            queue.Enqueue('c');
            queue.Enqueue('d');
            Console.WriteLine("Currently queue has {0} elements", queue.Count);
            Console.WriteLine("Queue has begining element :  {0}",queue.Peek());
            foreach (var element in queue)
            {
                Console.WriteLine(element);
            }
            Console.WriteLine(queue.Dequeue());
            char c = (char)queue.Dequeue();
            if (queue.Contains('a'))
            {
                Console.WriteLine("Exists!!");
            }
            else
            {
                Console.WriteLine("Not Exist!!");
            }
            Console.WriteLine("Finally the queue has {0} elements", queue.Count);
            Console.WriteLine();
        }
    }
}
