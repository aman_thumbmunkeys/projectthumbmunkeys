﻿using System;
using System.Collections.Generic;

namespace WorkArena.Collections
{
    class SortedSetUsage
    {
        public SortedSetUsage()
        {
            int n;
            Console.WriteLine("Enter the number to be enterd in sorted list: ");
            n=int.Parse(Console.ReadLine());
            SortedSet<int> rollNumber=new SortedSet<int>();
            SortedSet<int> numberWise = new SortedSet<int>();

            rollNumber.Add(1);
            rollNumber.Add(5);
            rollNumber.Add(21);
            rollNumber.Add(256);
            SortedSet<string> rollNumberSet=new SortedSet<string>();
            rollNumberSet.Add("Y13uc021");
            rollNumberSet.Add("Y13uc256");
            rollNumberSet.Add("Y13uc253");
            Console.WriteLine("elements is set are : {0}", rollNumber.Count);
            Console.WriteLine("max is {0}",rollNumber.Max);
            Console.WriteLine("Min is {0}",rollNumber.Min);
            foreach (var number in rollNumber)
            {
                Console.WriteLine(number);
            }
            Console.WriteLine();
            foreach (var rollNum in rollNumberSet)
            {
                Console.WriteLine(rollNum);
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                numberWise.Add(int.Parse(Console.ReadLine()));
            }
            foreach (var print in numberWise)
            {
                Console.WriteLine(print);
            }
        }
    }
}
