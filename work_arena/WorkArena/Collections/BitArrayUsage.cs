﻿using System;
using System.Collections;

namespace WorkArena.Collections
{
    class BitArrayUsage
    {
        public void BitArrayImplementation()
        {
            BitArray bitArray1 = new BitArray(8);
            BitArray bitArray2 = new BitArray(8);
            byte[] value1 = { 60 };
            byte[] value2 = { 12 };
            bitArray1 = new BitArray(value1);
            bitArray1 = new BitArray(value1);
            bitArray1.SetAll(false);
            for (int i = 0; i < bitArray1.Count; i++)
            {
                Console.Write("{0} ",bitArray1[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < bitArray2.Count; i++)
            {
                Console.Write("{0} ",bitArray2[i]);
            }
            Console.WriteLine();
            BitArray bitArray3 = new BitArray(8);
            bitArray3 = bitArray1.And(bitArray2);
            for (int i = 0; i < bitArray3.Count; i++)
            {
                Console.Write("{0} ",bitArray3[i]);
            }
            Console.WriteLine();
            bitArray3 = bitArray1.Or(bitArray2);
            for (int i = 0; i < bitArray3.Count; i++)
            {
                Console.Write("{0} ",bitArray3[i]);
            }
            Console.WriteLine();
        }
    }
}
