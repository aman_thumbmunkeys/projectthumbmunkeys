﻿using System;
using System.Collections.Generic;

namespace WorkArena.Collections
{
    class SortedDictionaryUsage
    {
        public void DictionaryImplementation()
        {
            SortedDictionary<int, string> dictionary = new SortedDictionary<int, string>();
            dictionary.Add(1, "Aman");
            dictionary.Add(22, "Chirag");
            dictionary.Add(31, "Sid");
            dictionary.Add(4, "Harsh");
            foreach (KeyValuePair<int, string> names in dictionary)
            {
                Console.WriteLine("Key : {0} Value: {1}", names.Key, names.Value);
            }
            if (dictionary.ContainsKey(1))
            {
                Console.WriteLine("Exists ....");
            }
            else
            {
                Console.WriteLine("Not Exist !!");
            }
            if (dictionary.ContainsValue("Chirag"))
            {
                Console.WriteLine("Exist");
            }
            else
            {
                Console.WriteLine("Not Exist !!");
            }
            Console.WriteLine("For key 2 the value is {0}", dictionary[22]);
        }
    }
}
