﻿using System;
using System.Collections;
using System.Security.Cryptography;

namespace WorkArena.Collections
{
    class HashTableUsage
    {
        public void HashTableImplement()
        {
            Hashtable hashTable = new Hashtable();
            hashTable.Add(1, "Aman");
            hashTable.Add(5, "Chirag");
            hashTable.Add(6, "Pranoy");
            hashTable.Add(2, "Thumbmunkeys");
            if (hashTable.ContainsKey("2"))
            {
                Console.WriteLine("Key exists !!");
            }
            if (hashTable.ContainsValue("Sharma"))
            {
                Console.WriteLine("Alreday Exists in hash table");
            }
            else
            {
                hashTable.Add(3,"sharma");
            }
            hashTable.Add(4, "abc");
            ICollection key = hashTable.Keys;
            ICollection value = hashTable.Values;
            Console.WriteLine("number of key pair in hash table {0}",hashTable.Count);
            hashTable.Remove(3);
            foreach (var k in value)
            {
                Console.WriteLine(k + ":" + hashTable[value] );
            }
            hashTable.Clear();
            Console.ReadLine();

        }
    }
}
