﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;

namespace WorkArena.Collections
{
    class SortedListUsage
    {
        public void Implementation()
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(1, "AMAN");
            sortedList.Add(45, "SID");
            sortedList.Add(4, "HARSH");
            sortedList.Add(5, "CHIRAG");
            if (sortedList.ContainsValue("thumbmunkeys"))
            {
                Console.WriteLine("value Exists !!");
            }
            else
            {
                sortedList.Add(23,"thumbmunkeys");
            }
            IList myKeyList = sortedList.GetKeyList();
            IList myValueList = sortedList.GetValueList();
            ICollection key = sortedList.Keys;
            ICollection value = sortedList.Values;
            foreach (var i in sortedList.Keys)
            {
                Console.WriteLine(i);
            }
            for (int i = 0; i < sortedList.Count; i++)
            {
                Console.WriteLine(myValueList[i]);
            }
            for (int i = 0; i < sortedList.Count; i++)
            {
                Console.WriteLine(myKeyList[i]);
            }
        }
    }
}
