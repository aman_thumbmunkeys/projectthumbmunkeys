﻿using System;
using System.Collections.Generic;
using WorkArena.Aggregation;

namespace WorkArena.Collections
{
    class HashSetUsage
    {
        public void HashSetImplementation()
        {
            HashSet<string> hashSet = new HashSet<string>
            {
                "Aman",
                "thumbmunkeys",
                "Chirag",
                "Aman"
            };
            HashSet<string> implementHashSet = new HashSet<string>();
            implementHashSet.Add("A1");
            implementHashSet.Add("A2");
            Console.WriteLine("elements in hash set1 are: {0}", hashSet.Count);
            Console.WriteLine("elements in hash set2 are: {0}", implementHashSet.Count);
            foreach (var name in hashSet)
            {
                Console.WriteLine(name);
            }
            if (hashSet.Contains("Chirag"))
            {
                hashSet.Remove("Chirag");
            }
            foreach (var name in implementHashSet)
            {
                Console.WriteLine(name);
            }
            if (hashSet.Contains("Aman"))
            {
                Console.WriteLine("Hash set has Aman in it...");
            }
            PassingHashSet(hashSet);
        }
        public void PassingHashSet(HashSet<string> set)
        {
            foreach (var hashValue in set)
            {
                Console.WriteLine(hashValue);
            }
        }
    }
}
