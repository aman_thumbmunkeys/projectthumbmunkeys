﻿using System;
using System.Collections.Generic;
namespace WorkArena.Collections
{
    class ListExample
    {
        public ListExample()
        {
            string[] grades= {"A","B","C","D"};
            ;
            List<string> listExample = new List<string>
            {
                "Aman",
                "Chirag",
                "Sid"
            };
            List<int> list=new List<int>{1,5,6,7,2};
            List<string> stringList =new List<string>(grades);
            Console.WriteLine("Maximum internal data structure can hold without resizing : {0}",list.Capacity);
            Console.WriteLine("length of list is : {0}", list.Count);
            foreach (var name in listExample)
            {
                Console.Write("{0} ",name);
            }
            Console.WriteLine();
            foreach (var integer in list)
            {
                Console.Write("{0} ",integer);
            }
            Console.WriteLine();
            foreach (var liststring in stringList)
            {
                Console.Write("{0} ",liststring);
            }
            Console.WriteLine();
            for (int i = 0; i < stringList.Count; i++)
            {
                Console.Write("{0} ",stringList[i]);
            }
            Console.WriteLine();
        }
    }
}
