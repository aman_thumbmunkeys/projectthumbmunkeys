﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace WorkArena.Collections
{
    class DictionaryUsage
    {
        public void DictionaryImplementation()
        {
            Dictionary<int,string> dictionary=new Dictionary<int, string>();
            dictionary.Add(1,"Aman");
            dictionary.Add(2,"Chirag");
            dictionary.Add(3, "Sid");
            dictionary.Add(4,"Harsh");
            foreach (KeyValuePair<int,string> names in dictionary)
            {
                Console.WriteLine("Key : {0} Value: {1}",names.Key,names.Value);
            }
            if (dictionary.ContainsKey(1))
            {
                Console.WriteLine("Exists ....");
            }
            else
            {
                Console.WriteLine("Not Exist !!");
            }
            if (dictionary.ContainsValue("Chirag"))
            {
                Console.WriteLine("Exist");
            }
            else
            {
                Console.WriteLine("Not Exist !!");
            }
            Console.WriteLine("For key 2 the value is {0}",dictionary[2]);
        }
    }
}
