﻿using System;
using System.Collections.Generic;
namespace WorkArena.Collections
{
    class StackUsage
    {
        public void StackImplementation()
        {
            int max = 0;
            Stack<int> stack=new Stack<int>(4);
            stack.Push(67);
            stack.Push(4);
            stack.Push(8);
            stack.Push(89);
            foreach (var number in stack)
            {
                if (number >= max)
                {
                    max = number;
                }
            }
            stack.Push(67);
            foreach (var element in stack)
            {
                Console.WriteLine(element);
            }
            Console.WriteLine("Elements in stack are : {0}",stack.Count);
            Console.WriteLine("maximum element is : {0}",max);
            Console.WriteLine("peek element is: {0}",stack.Peek());
            Console.WriteLine("Element popped is; {0}",stack.Pop());
            Console.WriteLine();
        }
    }
}
