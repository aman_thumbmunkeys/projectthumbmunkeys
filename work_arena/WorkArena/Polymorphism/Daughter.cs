﻿using System;

namespace WorkArena.Polymorphism
{
    class Daughter : Father
    {
        public override void Age()
        {
            Console.WriteLine("My age is : {0}", MyAge - 22);
        }
    }
}
