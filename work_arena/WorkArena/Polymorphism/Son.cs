﻿using System;

namespace WorkArena.Polymorphism
{
    class Son : Father
    {
        public override void Age()
        {
            Console.WriteLine("My age is : {0}", MyAge - 20);
        }
    }
}
