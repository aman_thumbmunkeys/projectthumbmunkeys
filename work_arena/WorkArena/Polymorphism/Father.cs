﻿using System;

namespace WorkArena.Polymorphism
{
    class Father
    {
        public int MyAge = 44;
        public void WorkDetails()
        {
            Console.WriteLine("I ama developer...");
        }
        public virtual void Age()
        {
            Console.WriteLine("I have Two Childs!! my age is :{0} ", MyAge);
        }
    }
}
