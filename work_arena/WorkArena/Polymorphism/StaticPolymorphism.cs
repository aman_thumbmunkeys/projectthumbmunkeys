﻿using System;

namespace WorkArena.Polymorphism
{
    class StaticPolymorphism
    {
        public void GetNumber(int number)
        {
            Console.WriteLine("Number is : {0}", number);
        }
        public void GetNumber(float number)
        {
            Console.WriteLine("float number is : {0}", number);
        }
        public void GetNumber(string number)
        {
            int j = int.Parse(number);
            Console.WriteLine("Number is {0}", j);
            Console.WriteLine();
        }
    }
}
