﻿using System;

namespace WorkArena.Enums
{
    class EnumUsage
    {
        enum Days { Sun,Mon,Tue,Wed,Thur,Fri,Sat};
        enum Seasons {Summer,Spring,Winter,Fall };

        public void EnumValues()
        {
            int WeekDayStart = (int)Days.Mon;
            int WeekDayEnd = (int) Days.Fri;
            int SeasonStart = (int) Seasons.Summer;
            int SeasonEnd = (int) Seasons.Fall;
            Console.WriteLine("Monday: {0}",WeekDayStart);
            Console.WriteLine("Friday : {0}",WeekDayEnd);
            Console.WriteLine("Monday: {0}",SeasonStart);
            Console.WriteLine("Monday: {0}", SeasonEnd);
            Console.ReadLine();
        }
    }
}
