﻿using System;

namespace WorkArena.Encapsulation
{
    class PrivateAcccessSpecifierUsage
    {
        private int _length;
        private int _width;
        public void GetLenth()
        {
            Console.WriteLine("Enter length : ");
            _length = Convert.ToInt32(Console.ReadLine());
        }

        public void GetWidth()
        {
            Console.WriteLine("Enter Width :");
            _width = Convert.ToInt32(Console.ReadLine());
        }

        public void DisplayArea()
        {
            Console.WriteLine("Area is :{0}", _length * _width);
            Console.ReadLine();
        }
    }
}
