﻿#define Dbug
#define Code
using System;

namespace WorkArena.PreprocessorDirectives
{
    class PreprocessorDirectivesUsage
    {
        public PreprocessorDirectivesUsage()
        {
#if Dbug
            Console.WriteLine("Preprocessor Present ...");
#if Code
            Console.WriteLine("Preprocessor present....");
#if (Dbug && Code)
            Console.WriteLine("Both present...");

#endif
#endif
#endif
        }
    }
}
