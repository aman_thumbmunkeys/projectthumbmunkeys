﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WorkArena.Abstraction;
using WorkArena.Aggregation;
using WorkArena.ArrayString;
using WorkArena.Attributes;
using WorkArena.Collections;
using WorkArena.ConstructorDestructor;
using WorkArena.CSharpPrograms;
using WorkArena.DateAndTime;
using WorkArena.Delegates;
using WorkArena.DelegatesAndEvents;
using WorkArena.DynamicPolymorphism;
using WorkArena.Encapsulation;
using WorkArena.Enums;
using WorkArena.ExceptionHandling;
using WorkArena.ExtensionMethods;
using WorkArena.FileInputOutput;
using WorkArena.Indexers;
using WorkArena.Inheritance;
using WorkArena.Interfaces;
using WorkArena.LinqQuery;
using WorkArena.Multithreading;
using WorkArena.OperatorOverloading;
using WorkArena.Overloading;
using WorkArena.OverridingBasicConcept;
using WorkArena.PluralSightCourses.CollectionsAndGenerics;
using WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse;
using WorkArena.PluralSightCourses.InterfacesCourse.AbstractClassUsage;
using WorkArena.PluralSightCourses.InterfacesCourse.ConcreteClassUsage;
using WorkArena.PluralSightCourses.InterfacesCourse.ExplicitImplementation;
using WorkArena.PluralSightCourses.InterfacesCourse.InterfaceUsage;
using WorkArena.PluralSightCourses.LinqCourse;
using WorkArena.PluralSightCourses.OOPCourse;
using WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface;
using WorkArena.Polymorphism;
using WorkArena.PreprocessorDirectives;
using WorkArena.PropertiesUse;
using WorkArena.RegularExpressions;
using WorkArena.SealedKeyword;
using WorkArena.StaticAndThis;
using WorkArena.Structs;
using WorkArena.Structure;
using WorkArena.WhileForUse;
using WorkArena.YieldKeyword;
using Address = WorkArena.Aggregation.Address;
using Customer = WorkArena.PluralSightCourses.DelegatesEventsLamdasCourse.Customer;

namespace WorkArena
{
    class ClassMain
    {
        public static object PropertyUsage { get; private set; }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(" Enter 1 to do array and string stuff:\n" +
                                  " Enter 2 to do Constructor Stuff:\n" +
                                  " Enter 3 to do WhileFor Use:\n" +
                                  " Enter 4 to see use of This keyword:\n" +
                                  " Enter 5 to see Static keyword use:\n" +
                                  " Enter 6 to See Structs use:\n" +
                                  " Enter 7 to See Enum Implementation:\n" +
                                  " Enter 8 to see Date and time program:\n" +
                                  " Enter 9 to do Inheritance:\n" +
                                  " Enter 10 to do Aggregation:\n" +
                                  " Enter 11 to see properties Usage\n" +
                                  " Enter 12 to see overloading\n" +
                                  " Enter 13 to see basic overriding\n" +
                                  " Enter 14 to see BAsic Polymorphism\n" +
                                  " Enter 15 to see static polymorphism(Compile time or function overloading)\n" +
                                  " Enter 16 For Dynamic Polymorphism(runtime polymorphism)\n" +
                                  " Enter 17 to see use of sealed keyword\n" +
                                  " Enter 18 to perform operator overloading\n" +
                                  " Enter 19 to understand Private Access/Encapsulation\n" +
                                  " Enter 20 for abstraction\n" +
                                  " Enter 21 to see Interface Implementation\n" +
                                  " Enter 22 for Throw Usage(Exception Handling)\n" +
                                  " Enter 23 for list implementaion\n" +
                                  " Enter 24 for Extension method implementation\n" +
                                  " Enter 25 for hashset implementation\n" +
                                  " Enter 26 for sorted set implementation\n" +
                                  " Enter 27 for hash table implementation results\n" +
                                  " Enter 28 for stack: \n" +
                                  " Enter 29 for sorted list\n" +
                                  " Enter 30 for Queue :\n" +
                                  " Enter 31 for BitArray :\n" +
                                  " Enter 32 for Linked list" +
                                  " Enter 33 For Dictionary usage: \n" +
                                  " Enter 34 For Sorted Dictionary :\n" +
                                  " Enter 35 for FileStream : \n" +
                                  " Enter 36 For preprocessor :\n" +
                                  " Enter 37 for Regular expression: \n" +
                                  " Enter 38 for Delegates:\n" +
                                  " Enter 39 for anonymous method and Lamda Expression:\n" +
                                  " Enter 40 for Linq Usage :\n" +
                                  " Enter 41 for Reflection Usage:\n" +
                                  " Enter 42 for attribute usage: \n" +
                                  " Enter 43 for delegates (Pluaral sight Course)\n" +
                                  " Enter 44 for Events (Pluaral sight Course)\n" +
                                  " Enter 45 for custom delegate with lamdas:\n" +
                                  " Enter 46 for Array Collection and generic class Usage: \n" +
                                  " Enter 47 for Generic class ,delegate and method Usage: \n" +
                                  " Enter 48 for Generic List:\n" +
                                  " Enter 49 for basic Base and derived class relationship:\n" +
                                  " Enter 50 for Interface in oop pluralsight:\n" +
                                  " Enter 51 for Interface ,abstract, Concrete class\n" +
                                  " Enter 52 for Extension methods plural sight . \n" +
                                  " Enter 53 for Threading \n" +
                                  " Enter 54 for Indexers :\n" +
                                  " Enter 55 for Reverse a number: \n" +
                                  " Enter 56 for Fibonacci: \n" +
                                  " Enter 57 for Prime Number Check \n" +
                                  " Enter 58 for Factorial : \n" +
                                  " Enter 59 for Armstrong number check: \n" +
                                  " Enter 60 For Deciaml to binary Convert : \n" +
                                  " Enter 61 for Linq Usage Plural sight: \n" +
                                  " Enter 62 for Yield keyword USage :");
                int n;
                var success = int.TryParse(Console.ReadLine(), out n);
                if (!success)
                {
                    Console.WriteLine("invalid input entered");
                    Console.ReadLine();
                    return;
                }
                switch (n)
                {
                    case 1:
                        {
                            int[] arr = { 1, 2, 3, 4, 5 };
                            var array = new ExampleArray();
                            array.PrintArray();
                            array.PrintString();
                            array.ReverseString();
                            array.StringReverse();
                            var arrayMaximum = new ArrayToFunction();
                            arrayMaximum.ArrayMaxElement(arr);
                            var methods = new StringBuiltInMethods();
                            methods.StringMethods();
                        }
                        break;

                    case 2:
                        {
                            var constructor = new ExampleConstructor();
                        }
                        break;
                    case 3:
                        {
                            var useWhile = new WhileForUsage();
                            useWhile.Display();
                        }
                        break;
                    case 4:
                        {
                            var callThisUse = new ThisUsage(22, "thumbmunkeys", 8.6f);
                            callThisUse.DisplayFields();
                        }
                        break;
                    case 5:
                        {
                            Account.RateOfInterest = 7.8f;
                            var getDetails = new Account();
                            getDetails.AccountDetails(1234556, "Aman");
                            getDetails.DisplayAccountDetails();
                        }
                        break;
                    case 6:
                        {
                            Circle circleStructs;
                            circleStructs.Radius = 10;
                            circleStructs.Pi = 3.14f;
                            Console.WriteLine("Area and radius are: {0} and {1}", circleStructs.Radius, circleStructs.Pi);
                            Console.WriteLine("Structure : ");
                            var structureUsage1 = new StructureUsage();
                            structureUsage1.GetDetails("aman", "LNMIIT", 21);
                            structureUsage1.Display();
                        }
                        break;
                    case 7:
                        {
                            var getEnum = new EnumUsage();
                            getEnum.EnumValues();
                        }
                        break;
                    case 8:
                        {
                            var dateMethod = new DateAndTimesProgram();
                            dateMethod.ShowDateAndTime();
                        }
                        break;
                    case 9:
                        {
                            var animal = new Dog();
                            animal.Bark();
                            animal.Eat();
                            animal.Run();
                            animal.Speak();
                            var bike = new HeroHonda();
                            bike.CalculateSpeed();
                            bike.BikeModelName();
                            bike.BikeType();
                            Console.WriteLine();
                        }
                        break;
                    case 10:
                        {
                            var getAddress = new Address("Tareen bahadur Ganj", "Noida", "UP");
                            var getEmployeeAdd = new EmployeeAddress(1, "Aman", getAddress);
                            getEmployeeAdd.Display();
                            Console.WriteLine();
                        }
                        break;
                    case 11:
                        {
                            var property = new PropertiesUsage("Aman", 22, 12.2f);
                            property.DisplayProperties();
                            var propertyUse = new PropertyUse("Aman", 22);
                            propertyUse.DisplayValues();
                        }
                        break;
                    case 12:
                        {
                            var overloading = new MethodOverloadingByArguments();
                            overloading.Area(3, 4);
                            overloading.Area(3, 4, 5);
                            overloading.Area(7);
                            var overload = new MethodOverloadigDataType();
                            overload.Area(3.14f, 4);
                            overload.Area(2, 7);
                            overload.Area(12.909, 23);
                        }
                        break;
                    case 13:
                        {
                            var rectangle = new Rectangle();
                            rectangle.Draw();
                            var shape = new Shape();
                            shape.Draw();
                        }
                        break;
                    case 14:
                        {
                            var father = new Father();
                            father.Age();
                            Father son = new Son();
                            son.Age();
                            Father daughter = new Daughter();
                            daughter.Age();
                        }
                        break;
                    case 15:
                        {
                            var polymorphism = new StaticPolymorphism();
                            polymorphism.GetNumber(12);
                            polymorphism.GetNumber(12.45f);
                            polymorphism.GetNumber("12");
                        }
                        break;
                    case 16:
                        {
                            var rectangle = new RectangleD(12, 34);
                            rectangle.Area();
                            var triangle = new TriangleD(10, 12);
                            triangle.Area();

                        }
                        break;
                    case 17:
                        {
                            var midterm = new MidTermExam();
                            midterm.Result();
                        }
                        break;
                    case 18:
                        {
                            var cuboidVolume1 = new CuboidVolume();
                            cuboidVolume1.SetLength(12);
                            cuboidVolume1.SetBreadth(4);
                            cuboidVolume1.SetHeight(15);
                            var cuboidVolume2 = new CuboidVolume();
                            cuboidVolume2.SetLength(5);
                            cuboidVolume2.SetBreadth(10);
                            cuboidVolume2.SetHeight(14);
                            cuboidVolume1.GetVolume();
                            cuboidVolume2.GetVolume();
                            var cuboidVolume3 = cuboidVolume1 + cuboidVolume2;
                            cuboidVolume3.GetVolume();
                            Console.ReadLine();

                        }
                        break;
                    case 19:
                        {
                            var privateAcccess = new PrivateAcccessSpecifierUsage();
                            privateAcccess.GetLenth();
                            privateAcccess.GetWidth();
                            privateAcccess.DisplayArea();
                        }
                        break;
                    case 20:
                        {
                            School parents = new Parents();
                            parents.Marks();
                            parents.Grades();
                            School teacher = new Teacher();
                            teacher.Marks();
                            teacher.Grades();
                        }
                        break;
                    case 21:
                        {
                            var onlineTransaction = new OnlineTransaction("0012", "Union Bank", 20000);
                            onlineTransaction.ShowTransaction();
                        }
                        break;
                    case 22:
                        {
                            var throwUsage = new TryCatchFinallyUsage();
                            throwUsage.CatchException();
                            var userException = new UserExceptionImplementation();
                            userException.GetCab();
                        }
                        break;
                    case 23:
                        {
                            var listExample = new ListExample();
                        }
                        break;
                    case 24:
                        {
                            var extensionMethod = new ExtensionMethodUsage();
                        }
                        break;
                    case 25:
                        {
                            var hashSet = new HashSetUsage();
                            hashSet.HashSetImplementation();
                        }
                        break;
                    case 26:
                        {
                            var sortedUsage = new SortedSetUsage();
                        }
                        break;
                    case 27:
                        {
                            var hashTable = new HashTableUsage();
                            hashTable.HashTableImplement();
                        }
                        break;
                    case 28:
                        {
                            var stackUsage = new StackUsage();
                            stackUsage.StackImplementation();
                        }
                        break;
                    case 29:
                        {
                            var sortedList = new SortedListUsage();
                            sortedList.Implementation();
                        }
                        break;
                    case 30:
                        {
                            var queue = new QueueUsage();
                            queue.QueueImplementation();

                        }
                        break;
                    case 31:
                        {
                            var bitArray = new BitArrayUsage();
                            bitArray.BitArrayImplementation();
                        }
                        break;
                    case 32:
                        {
                            var linkedList = new LinkedListUsage();
                            linkedList.LinkedListFunctions();
                            Console.WriteLine();
                        }
                        break;
                    case 33:
                        {
                            var dictionaryUsage = new DictionaryUsage();
                            dictionaryUsage.DictionaryImplementation();
                            Console.WriteLine();
                        }
                        break;
                    case 34:
                        {
                            var sortedDictionary = new SortedDictionaryUsage();
                            sortedDictionary.DictionaryImplementation();
                            Console.WriteLine();
                        }
                        break;
                    case 35:
                        {
                            var fileStream = new FileStreamUsage();
                            fileStream.ReadFile();
                            var serializeUsage = new SerializationUsage();
                            var deserializationUsage = new DeserializationUsage();
                        }
                        break;
                    case 36:
                        {
                            var preprocessorDirectives = new PreprocessorDirectivesUsage();
                        }
                        break;
                    case 37:
                        {
                            var regularExpression = new RegularExpressionUsage();
                        }
                        break;
                    case 38:
                        {
                            var delegatesUsage = new DelegatesUsage();
                            CalulatorDelegate calculatorDelegate = (delegatesUsage.Add);
                            calculatorDelegate += delegatesUsage.Subtract;
                            calculatorDelegate += delegatesUsage.Multiply;

                            Console.WriteLine("Subtraction method is removed by -= operator !! ....\n");
                            calculatorDelegate -= delegatesUsage.Subtract;
                            calculatorDelegate.Invoke(12, 34);

                            var resultEvent = new ResultEvent();
                            resultEvent.CalculatorEvent += NotifyUser1.NotifyHandler;
                            resultEvent.CalculatorEvent += NotifyUser2.Notifyhandler2;
                            resultEvent.NotifyUser(12, 34);
                        }
                        break;
                    case 39:
                        {
                            CalulatorDelegate delegates;
                            CalulatorDelegate delegatesLamda;
                            delegates = delegate (int x, int y)
                            {
                                Console.WriteLine("Anonymous method used  & division is : {0}  ", x / y);
                                Console.WriteLine();
                            };
                            delegates.Invoke(10, 5);
                            delegatesLamda = (x, y) => { Console.WriteLine("Using Lamda Expressions : {0} ", x / y); };
                            delegatesLamda.Invoke(12, 7);
                            Console.WriteLine();
                        }
                        break;
                    case 40:
                        {
                            var linkUsage = new LinkUsage();
                            linkUsage.CheckVowel();
                        }
                        break;
                    case 41:
                        {
                            var reflectionUsage = new ReflectionUsage();
                            reflectionUsage.UsingReflection();
                        }
                        break;
                    case 42:
                        {
                            var attributesUsage = new AttributesUsage();
                            var sum = attributesUsage.Add(new List<int>() { 10, 20, 30 });
                            Console.WriteLine("sum is : {0} ", sum);
                            var usingDebug = new UsingDebugInfo();
                            usingDebug.UsingDebugInfoMethod();
                            var testAttribute = new TestAttribute();
                            testAttribute.AccessAttribute();
                        }
                        break;
                    case 43:
                        {
                            var creatingDelegates = new CreatingDelegates();
                            WorkPerformedHandler workPerformed1 = creatingDelegates.Work1;
                            WorkPerformedHandler workPerformed2 = creatingDelegates.Work2;
                            WorkPerformedHandler workPerformed3 = creatingDelegates.Work3;

                            workPerformed1 += workPerformed2 + workPerformed3;
                            workPerformed1.Invoke(5, WorkType.CricketPlayed);
                        }
                        break;
                    case 44:
                        {
                            var workerObj = new Worker();
                            Console.WriteLine("Event Work event is attached to event handler....");
                            workerObj.WorkEvent += Worker.WorkShow;
                            Console.WriteLine("Calling event handler using lamdas:...");
                            workerObj.WorkEvent += (s, e) =>
                            {
                                Console.WriteLine("Worked hours and type is : ");
                                Console.WriteLine($"{e.Hours},{e.WorkType}");

                            };
                            workerObj.WorkCompletedEvent += Worker.WorkCompleted;
                            workerObj.DoWork(8, WorkType.GoTomeetings);
                        }
                        break;
                    case 45:
                        {
                            DelegateProcess addDelegateProcess = (x, y) => (x + y);
                            DelegateProcess mulDelegateProcess = (x, y) => (x * y);
                            var dataProcess = new ProcessDataUsingCustomDelegate();
                            dataProcess.DataProcess(12, 10, addDelegateProcess);
                            dataProcess.DataProcess(15, 20, mulDelegateProcess);

                            Action<int, int> addAction = (x, y) => Console.WriteLine(x + y);
                            Action<int, int> mulAction = (x, y) => Console.WriteLine(x * y);

                            Console.WriteLine("Add action is performed : ");
                            dataProcess.ActionProcess(12, 12, addAction);
                            Console.WriteLine("Multiply action is performed : ");
                            dataProcess.ActionProcess(10, 5, mulAction);

                            Func<string, string, string> addString = (x, y) => x + y;
                            Func<int, int, int> addFunc = (x, y) => x + y;
                            Func<int, int, int> mulFunc = (x, y) => x * y;
                            dataProcess.StringFunc("aman", "Sharma", addString);

                            Console.WriteLine("Func add delegate is invoked :  ");
                            dataProcess.FuncProcess(5, 6, addFunc);
                            Console.WriteLine("Func multiply delegate is invoked : ");
                            dataProcess.FuncProcess(4, 8, mulFunc);

                            var customer = new List<Customer>
                            {
                                new Customer{City="SPn",FirstName = "Aman",Id = 21,LastName = "Sharma"},
                                new Customer{City="BE",FirstName = "Chirag",Id = 7,LastName = "Sharma"}
                            };

                            var customerProcess = customer.Where(c => c.City == "Spn").OrderBy(c => c.FirstName);
                            foreach (var cust in customer)
                            {
                                Console.WriteLine(cust.FirstName);
                            }
                        }
                        break;
                    case 46:
                        {
                            var arrayCollectionUsage = new ArrayCollectionUsage();
                            var intArray = new CustomGenericArray<int>(5);
                            for (var i = 0; i < 5; i++)
                            {
                                intArray.SetItem(i, i + 5);
                            }
                            for (var i = 0; i < 5; i++)
                            {
                                Console.WriteLine(intArray.GetItem(i) + " ");
                            }
                            var charArray = new CustomGenericArray<char>(5);
                            for (var i = 0; i < 5; i++)
                            {
                                charArray.SetItem(i, (char)(i + 97));
                            }
                            for (var i = 0; i < 5; i++)
                            {
                                charArray.GetItem(i);
                            }
                        }
                        break;
                    case 47:
                        {
                            int a = 10, b = 20;
                            char c = 'A', d = 'Z';
                            var genericMethod = new GenericMethodUsage();
                            genericMethod.Swap(10, 20);
                            genericMethod.Swap(c, d);
                            int Addfunc(int x, int y) => (x + y);
                            genericMethod.Addition(12, 23, Addfunc);
                            var genericDelegate = new GenericDelegateUsage();
                            NumberChanger<int> numberChanger1 = (genericDelegate.Addition);
                            NumberChanger<int> numberChanger2 = (genericDelegate.Multiplication);
                            numberChanger1.Invoke(12, 23);
                            numberChanger2(10, 40);
                            NumberChanger<int> numberChanger3 = (genericDelegate.Swap);
                            numberChanger3.Invoke(12, 56);
                            Console.WriteLine();
                        }
                        break;
                    case 48:
                        {
                            var genericList = new GenericListUsage();
                            genericList.ListImplementation();
                            var genericDictionaries = new GenericDictionariesUsage();
                        }
                        break;
                    case 49:
                        {
                            var bankingTransactions = new BankingTransactions(80000, 2000);
                            bankingTransactions.MiniStatement();
                            bankingTransactions.BalanceAfterWithdrawl();
                        }
                        break;

                    case 50:
                        {
                            var changedItems = new List<ILoggable>();
                            var customer = new CustomerForInterface()
                            {
                                Email = "amanspn94@gmail.com",
                                FirstName = "Aman",
                                LastName = "Sharma"
                            };
                            changedItems.Add(customer);

                            var product = new ProductForInterface()
                            {
                                ProductDescription = "Product is Good..",
                                ProductName = "Cricket Bat",
                                CurrentPrice = 2500,
                            };
                            changedItems.Add(product);
                            LoggingService.WriteToFile(changedItems);
                        }
                        break;
                    case 51:
                        {
                            var regularSquare = new RegularSquare(6);
                            regularSquare.GetPerimeter();
                            regularSquare.GetArea();

                            var regularTriangle = new RegularTriangle(11);
                            regularTriangle.GetPerimeter();
                            regularTriangle.GetArea();

                            var octagonToImplement = new OctagonToImplementPolygon(10);
                            octagonToImplement.GetPerimeter();
                            octagonToImplement.GetArea();
                        }
                        break;
                    case 52:
                        {
                            ISaveable saveable = new ImplementSaveablePrintable();
                            saveable.Save();
                            IPrintable printable = new ImplementSaveablePrintable();
                            printable.Save();
                            var explicitInterface = new ExplicitSaveableInterfaceUsage();
                            saveable = new ExplicitSaveableInterfaceUsage();
                            printable = new ExplicitSaveableInterfaceUsage();
                            explicitInterface.Save();
                            saveable.Save();
                            printable.Save();
                            ((ISaveable)explicitInterface).Save();
                            ((IPrintable)explicitInterface).Save();
                        }
                        break;
                    case 53:
                        {
                            var threading = new ThreadingUsage();
                            var t1Thread = new Thread(threading.ThreadNaming);
                            var t2Thread = new Thread(threading.ThreadNaming);
                            t2Thread.Name = "Thread 2";
                            t1Thread.Name = "thread 1";
                            t1Thread.Start();
                            t2Thread.Start();
                            t1Thread.Priority = ThreadPriority.Highest;
                            t2Thread.Priority = ThreadPriority.Lowest;

                            var threadingUsage = new ThreadingUsage();
                            var t1 = new Thread(threadingUsage.ThreadImplementaion);
                            var t2 = new Thread(threadingUsage.ThreadImplementaion);
                            t1.Start();
                            t2.Start();
                            try
                            {
                                t1.Abort();
                                t2.Abort();
                            }
                            catch (ThreadAbortException e)
                            {
                                Console.WriteLine(e.ToString());
                                throw;
                            }
                            Console.WriteLine("End Thread here ...");
                        }
                        break;
                    case 54:
                        {
                            var indexer = new IndexersImplementation();
                            indexer[0] = "aman";
                            indexer[1] = "Sharma";
                            for (var i = 0; i < IndexersImplementation.Size; i++)
                            {
                                Console.WriteLine(indexer[i]);
                            }
                        }
                        break;
                    case 55:
                        {
                            Console.WriteLine(" Reverse Number  : ");
                            var reverseNumber = new ReverseNumber();
                            reverseNumber.Reverse(12345);
                        }
                        break;
                    case 56:
                        {
                            Console.WriteLine("For Fibonacci series => Enter number of elements :");
                            var fibonacciSeries = new FibonacciSeries();
                            var limit = int.Parse(Console.ReadLine());
                            fibonacciSeries.FibonacciPrint(limit);
                        }
                        break;
                    case 57:
                        {
                            Console.WriteLine("Prime Number Check : Please Enter Number to check: ");
                            var number = int.Parse(Console.ReadLine());
                            var primeNumber = new PrimeNumber();
                            primeNumber.PrimeNumberCheck(number);
                        }
                        break;
                    case 58:
                        {
                            Console.WriteLine("Factorial of number: Enter Number to Find factorial: ");
                            var num = int.Parse(Console.ReadLine());
                            var factorial = new FactorialOfNumber();
                            factorial.FindFactorial(num);
                        }
                        break;
                    case 59:
                        {
                            Console.WriteLine("Armstrong number check : Enter number to check :");
                            var number = int.Parse(Console.ReadLine());
                            var armstrongNumber = new ArmstrongNumber();
                            armstrongNumber.ArmstrongNumberCheck(number);
                        }
                        break;
                    case 60:
                        {
                            Console.WriteLine("Decimal to binary: Enter Decimal number; ");
                            var decimalToBinary = new DecimalToBinary();
                            decimalToBinary.ConvertDecimalToBinary();
                        }
                        break;
                    case 61:
                        {
                            var customerRepository = new CustomerRepository();
                            var customerList = customerRepository.RetrieveFromCustomer();
                            customerRepository.FindCustomerDetails(customerList, 1);

                            var result = customerRepository.SortByName(customerList);
                            foreach (var item in result)
                            {
                                Console.WriteLine(item.LastName);

                            }
                        }
                        break;
                    case 62:
                        {
                            var yieldUsage = new YieldUsage();
                            IEnumerable<int> number=yieldUsage.Fibonacci(8);
                            foreach(var num in number)
                            {
                                Console.WriteLine(num);
                            }
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Wrong choice entered!!");
                        }
                        break;
                }
            }
        }
    }
}
