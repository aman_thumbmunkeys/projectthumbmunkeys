﻿namespace WorkArena.Structs
{
    public struct Circle
    {
        public int Radius;
        public float Pi;
        public Circle(int radius, float pi)
        {
            Radius = radius;
            Pi = pi;
        }
    }
}
