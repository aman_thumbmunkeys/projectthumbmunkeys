﻿using System;

namespace WorkArena.PropertiesUse
{
    class PropertiesUsage
    {
        public float Marks { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public PropertiesUsage(string name, int age, float marks)
        {
            Name = name;
            Age = age;
            Marks = marks;
        }

        public void DisplayProperties()
        {
            Console.WriteLine("Name is : {0}",Name);
            Console.WriteLine("age is : {0}", Age);
            Console.WriteLine("marks are : {0}", Marks);
        }
    }
}

