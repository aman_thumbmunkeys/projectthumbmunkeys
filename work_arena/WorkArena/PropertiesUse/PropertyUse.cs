﻿using System;
using System.CodeDom;

namespace WorkArena.PropertiesUse
{
    class PropertyUse
    {
        private string _name;
        private int _age;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value + "Sharma";
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value - 10;
            }
        }
        public PropertyUse(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public void DisplayValues()
        {
            Console.WriteLine("Full Name is :{0}", Name);
            Console.WriteLine("Age 10 years before is :{0}", Age);
            Console.WriteLine();
        }
    }
}







