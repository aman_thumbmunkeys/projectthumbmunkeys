﻿using System;
using System.Threading;

namespace WorkArena.Multithreading
{
    class ThreadingUsage
    {
        public void ThreadImplementaion()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
                Thread.Sleep(500);
            }
        }

        public void ThreadNaming()
        {
            Thread t = Thread.CurrentThread;
            Console.WriteLine(t.Name +" is Running... ");
        }
    }
}
