﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkArena.PluralSightCourses.OOPCourse;
using WorkArena.PluralSightCourses.OOPCourse.UnderstandingInterface;
using Customer = WorkArena.PluralSightCourses.OOPCourse.Customer;

namespace LoggingServiceTest
{
    [TestClass]
    public class LoggingTest
    {
        [TestMethod]
        public void LoggingServiceValidTest()
        {
            var changedItems = new List<ILoggable>();
            var customer = new CustomerForInterface()
            {
                Email = "amanspn94@gmail.com",
                FirstName = "Aman",
                LastName = "Sharma"
            };
            changedItems.Add(customer);

            var product = new ProductForInterface()
            {
                ProductDescription = "Product is Good..",
                ProductName = "Cricket Bat",
                CurrentPrice = 2500,
        };
        changedItems.Add(product);
        LoggingService.WriteToFile(changedItems);
        }
}
}
