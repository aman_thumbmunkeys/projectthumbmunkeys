﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringHandlerComponent;

namespace CommonComponentStringHandlerTest
{
    [TestClass]
    public class StringHandlerTest
    {
        [TestMethod]
        public void InputSpacesCheck()
        {
            var input = "AmanSharma";
            var expected = "Aman Sharma";
            var handler = new StringHandler();
            var actual = handler.InsertSpaces(input);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InsertSpacesWithExistingSpaces()
        {
            var input = "Aman Sharma";
            var expected = "Aman Sharma";
            var handler = new StringHandler();
            var actual = handler.InsertSpaces(input);
            Assert.AreEqual(expected, actual);
        }
    }
}
