﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkArena.PluralSightCourses.OOPCourse;

namespace ObjectOrientedProgrammingTest
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        [TestMethod]
        public void RetrieveExistingData()
        {
            var customerRepository = new CustomerRepositoryUsage();
            var expected = new Customer(2)
            {
                Email = "aman@thumbmunkeys.com",
                FirstName = "Aman",
                LastName = "Sharma"
            };
            var actual = customerRepository.Retrievedata(2);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.FirstName, actual.FirstName);
            Assert.AreEqual(expected.Email, actual.Email);
        }

        [TestMethod]
        public void RetrieveExistingWithAddress()
        {
            var customerRepository = new CustomerRepositoryUsage();
            var expected = new Customer(1)
            {
                Email = "ama@thumbmunkeys.com",
                FirstName = "Aman",
                LastName = "Sharma"
            };
            var addresslist = new List<Address>();
            var address = new Address(1)
            {
                StreetLine1 = "Jmd megapolis",
                City = "Gurgaon",
                State = "Haryana"
            };
            Address address2 = new Address(2)
            {
                StreetLine1 = "jmd megapolis",
                City = "Gurgaon",
                State = "Haryana"
            };
            var actual = customerRepository.Retrievedata(1);
            Assert.AreEqual(expected.LastName, actual.LastName);
            Assert.AreEqual(expected.Email, actual.Email);

            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(expected.AddressList[i].City, actual.AddressList[i].City);
                Assert.AreEqual(expected.AddressList[i].State, actual.AddressList[i].State);
            }
        }
    }
}
