﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkArena.PluralSightCourses.OOPCourse;
namespace ObjectOrientedProgrammingTest
{
    [TestClass]
    public class CustomerTest
    {
        [TestMethod]
        public void FullName()
        {
            Customer customer = new Customer(1);
            customer.FirstName = "Aman";
            customer.LastName = "Sharma";
            string expected = "Aman Sharma";
            string actual = customer.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FullNameFirstNameBlank()
        {
            Customer customer = new Customer(2);
            customer.LastName = "Sharma";
            string expected = " Sharma";
            string actual = customer.FullName;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void FullNameLastNameBlank()
        {
            Customer customer = new Customer(3);
            customer.FirstName = "Aman";
            string expected = "Aman ";
            string actual = customer.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValidFirstLastNameAndEmail()
        {
            Customer customer = new Customer(4);
            customer.FirstName = "Chirag";
            customer.LastName = "Sharma";
            customer.Email = "amanspn94@gmail.com";
            var expected = true;
            var actual = customer.Validate();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MissingFirstName()
        {
            Customer customer = new Customer(6);
            customer.LastName = "Sharma";
            customer.Email = "amanspn94@gmail.com";
            var expected = false;
            var actual = customer.Validate();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MissingFirstLastNameAndEmail()
        {
            Customer customer = new Customer(7);
            var expected = false;
            var actual = customer.Validate();
            Assert.AreEqual(expected,actual);
        }
    }
}
