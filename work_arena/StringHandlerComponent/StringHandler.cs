﻿namespace StringHandlerComponent
{
    public class StringHandler
    {
        public string InsertSpaces(string input)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(input))
            {
                foreach (char letter in input)
                {
                    if (char.IsUpper(letter))
                    {
                        result=result.Trim();
                        result += " ";
                    }
                    result += letter;
                }
                result = result.Trim();
            }
            return result;
        }
    }
}
