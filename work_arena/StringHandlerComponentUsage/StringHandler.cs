﻿using System;

namespace StringHandlerComponentUsage
{
    public class StringHandler
    {
        public string InsertSpaces(string input)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(input))
            {
                foreach (var letter in input)
                {
                    if (char.IsUpper(letter))
                    {
                        result.Trim();
                        result += " ";
                    }
                    result += result;
                }
                result += result.Trim();
            }
            return result;
        }
    }
}
