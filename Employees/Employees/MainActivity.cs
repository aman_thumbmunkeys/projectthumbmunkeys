﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Views;
using Java.Security;

namespace Employees
{
    [Activity(Label = "Employees", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ListActivity
    {
        public string[] Items;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Items=new[]{"Aman","Deepak","Chirag","Sid","Sidjr","Harsh","Roshan","Pranoy","Tanuj"};
            ListAdapter=new ArrayAdapter<string>(this,Android.Resource.Layout.SimpleListItem1,Items);
            
        }

        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            var itemValue = Items[position];
            //Toast.MakeText(this,t,ToastLength.Short).Show();
            Intent intent=new Intent(this,typeof(SecondActivity));
            intent.PutExtra("key",itemValue);
            StartActivity(intent);
        }
    }
}

