﻿using Android.App;
using Android.OS;
using Android.Widget;
using Java.IO;

namespace Employees
{
    [Activity(Label = "SecondActivity")]
    public class SecondActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EmployeeGreetings);
            var text = Intent.GetStringExtra("key") ?? "Data not available";
            TextView showText = FindViewById<TextView>(Resource.Id.textView1);
            showText.Text = showText.Text + " "+ text;
        }
    }
}